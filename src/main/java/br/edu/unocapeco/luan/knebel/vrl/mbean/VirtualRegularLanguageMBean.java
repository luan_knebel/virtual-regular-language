package br.edu.unocapeco.luan.knebel.vrl.mbean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientButtonControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumApplicationStage;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumTypeAutomata;
import br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions.FileException;
import br.edu.unocapeco.luan.knebel.vrl.architecture.resources.ApplicationResources;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.ValidationsUtils;
import br.edu.unocapeco.luan.knebel.vrl.beans.Grammar;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.beans.Transition;
import br.edu.unocapeco.luan.knebel.vrl.beans.TransitionTableInformation;
import br.edu.unocapeco.luan.knebel.vrl.controls.ApplicationStageControll;
import br.edu.unocapeco.luan.knebel.vrl.help.ApplicationHelp;
import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelState;
import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelViewAutomata;

public class VirtualRegularLanguageMBean extends AbstractClientControll	 {

	private Grammar grammar;
	
	private List<State> statesListAutomataGrammar;
	private List<State> statesListAutomataAFD;
	private List<State> statesListAutomataValidation;
	
	private DefaultTableModel tableModelTransitionTableGrammar;
	private DefaultTableModel tableModelTransitionTableAFD;
	private DefaultTableModel tableModelTransitionTableAFDSimplified;
	
	private TransitionTableInformation transitionTableInformation;
	private ApplicationStageControll applicationStageControll;
	private EnumTypeAutomata typeAutomata;
	private int axisXAutomataControl;
	private int axisYAutomataControl;
	
	private GrammarMBean grammarMBean;
	private FileMBean fileMBean;
	private TransitionTableMBean tableMBean;
	private TransformationAFNDToAFDMBean transformationAFNDToAFDMBean;
	private ValidateSentenceMBean validateSentenceMBean;

	public VirtualRegularLanguageMBean() {
		initObjects();
	}
	
	private void initObjects() {
		statesListAutomataGrammar = Collections.emptyList();
		statesListAutomataAFD = Collections.emptyList();
		statesListAutomataValidation = Collections.emptyList();
		
		tableModelTransitionTableGrammar = new DefaultTableModel();
		tableModelTransitionTableAFD = new DefaultTableModel();
		tableModelTransitionTableAFDSimplified = new DefaultTableModel();
		transitionTableInformation = new TransitionTableInformation();
		grammar = new Grammar();
	}

	public void validateClosingWindow() {
		try {
			getGrammarDataClient();
			fileMBean.setGrammar(grammar);
			fileMBean.validateSaveFileOnClosing();
		} catch (FileException e) {
			printMessageDialog(e.getMessage());
		}
	}
	
	public void createAFDSimplifiedTableAction() {
		
		try {
			statesListAutomataValidation =transformationAFNDToAFDMBean.transformAFDToAFDSimplified(statesListAutomataAFD);
			tableMBean.setStatesList(statesListAutomataValidation);
			tableModelTransitionTableAFDSimplified = tableMBean.createTableModelTransitions();
			transitionTableInformation = tableMBean.getTransitionTableInformation();
			updateApplicationStage(EnumApplicationStage.TABLE_AFD_SIMPLIFIED_GENERATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void helpAction() {
		ApplicationHelp.openHelp();
	}

	public void newAction() {
		
		try {
			fileMBean.newFile();
			grammar = fileMBean.getGrammar();
			clearClient();
			updateApplicationStage(EnumApplicationStage.GRAMMAR_INVALID);
			updateAFNDApplicationState(false);
		} catch (Exception e) {
			printMessageDialog(e.getMessage());
		}
	}
	
	public void openAction() {
		try {
			if (!fileMBean.readFile())return;
			clearClient();
			grammar = fileMBean.getGrammar();
			setGrammarDataClient();
			updateApplicationStage(EnumApplicationStage.GRAMMAR_INVALID);
			updateAFNDApplicationState(false);
		} catch (FileException e) {
			printMessageDialog(e.getMessage());
		}
	}

	public void saveAction() {
		try {
			getGrammarDataClient();
			fileMBean.setGrammar(grammar);
			fileMBean.saveFile();
		} catch (FileException e) {
			printMessageDialog(e.getMessage());
		}
	}
	
	public void aboutAction() {
		printMessageDialog(aboutVirtualRegularLanguage());
	}
	
	public void validateGrammarAction() {
		try {
			getGrammarDataClient();
			grammarMBean.setGrammar(grammar);
			grammarMBean.validateGrammar();
			printMessageDialog("Gramática regular validada!");
			updateApplicationStage(EnumApplicationStage.GRAMMAR_VALID);
		} catch (Exception e) {
			updateApplicationStage(EnumApplicationStage.GRAMMAR_INVALID);
			printMessageDialog(e.getMessage());
		}
	}
	
	public void validateCreateGrammarModifiedAction() {
		
		try {
			statesListAutomataGrammar = grammarMBean.getAutomataStructure();
			if(!grammar.isAFND()){
				statesListAutomataValidation = statesListAutomataGrammar;
			}
			String htmlGrammarWithNewStates = grammarMBean.getHtmlGrammarWithNewStates(statesListAutomataGrammar, grammar);
			getClientDataControll().getEditorPanelGramar().setText(htmlGrammarWithNewStates);
			updateApplicationStage(EnumApplicationStage.GRAMMAR_MODIFIED);
		} catch (Exception e) {
			e.printStackTrace();
			printMessageDialog(e.getMessage());
		}		
	}
	
	public void createTableGrammarTransitionAction() {
		
		try {
			tableMBean.setStatesList(statesListAutomataGrammar);
			tableModelTransitionTableGrammar = tableMBean.createTableModelTransitions();
			transitionTableInformation = tableMBean.getTransitionTableInformation();
			updateAFNDApplicationState(grammar.isAFND());
			updateApplicationStage(EnumApplicationStage.TABLE_GRAMMAR_GENERATED);
		} catch (Exception e) {
			e.printStackTrace();
			printMessageDialog(e.getMessage());
		}
	}
	
	public void createAFDTableAction() {
		
		try {
			transformationAFNDToAFDMBean.setOriginalStates(statesListAutomataGrammar);
			transformationAFNDToAFDMBean.transformAFNDToAFD();
			statesListAutomataAFD = transformationAFNDToAFDMBean.getAFDListStates(); 
			tableMBean.setStatesList(statesListAutomataAFD);
			tableModelTransitionTableAFD = tableMBean.createTableModelTransitions();
			updateApplicationStage(EnumApplicationStage.TABLE_AFD_GENERATED);
		} catch (Exception e) {
			printMessageDialog("Ocorreou um erro interno ao realizar esta operação!");
		}
		
	}
	
	public void createAutomataAFDAction() {
		
		clearPaneAutomataAFD();
		resetAxisXYAutomataControll();
		getClientDataControll().getPanelViewAutomata().setStates(statesListAutomataValidation);
		typeAutomata = EnumTypeAutomata.AFD;
		for (State state : statesListAutomataValidation) {
			
			if(!existTransactionForState(statesListAutomataValidation, state)) continue;
			addStateAtomata(state, getClientDataControll().getPanelViewAutomata());
		}
		updateApplicationStage(EnumApplicationStage.AUTOMATA_AFD_GENERATED);
		getClientDataControll().getPanelViewAutomata().repaint();
	}
	
	public void createAutomataAFNDAction() {
		
		clearPaneAutomataAFND();
		resetAxisXYAutomataControll();
		getClientDataControll().getPanelViewAutomataGrammar().setStates(statesListAutomataGrammar);
		typeAutomata = EnumTypeAutomata.AFND;
		for (State state : statesListAutomataGrammar) {
			
			if(!existTransactionForState(statesListAutomataGrammar, state)) continue;
			addStateAtomata(state, getClientDataControll().getPanelViewAutomataGrammar());
		}
		updateApplicationStage(EnumApplicationStage.AUTOMATA_AFND_GENERATED);
		getClientDataControll().getPanelViewAutomataGrammar().repaint();
	}

	private void resetAxisXYAutomataControll() {
		axisXAutomataControl = 5;
		axisYAutomataControl = 30;
	}
	
	private void addStateAtomata(State state, PanelViewAutomata panelViewAutomata) {
		
		if(axisXAutomataControl >= 400){
			axisXAutomataControl = 10;
			axisYAutomataControl += 80; 
		}
		
		PanelState panelState = new PanelState(state, this, typeAutomata);
		panelState.setBounds(axisXAutomataControl, axisYAutomataControl, panelState.getWidth(), panelState.getHeight());
		panelViewAutomata.add(panelState);
		axisXAutomataControl += 75;
	}
	
	private boolean existTransactionForState(List<State> listState, State stateFind) {
		if(stateFind.isInitial()) return true;
		if(stateFind.isTransationsEmpty() && !stateFind.isFinal()) return false;
		
		for (State state : listState) {
			for (Transition transition : state.getTransitions()) {
				if(transition.getDestinyState().equals(stateFind)) return true;
			}
		}
		
		return !stateFind.isTransationsEmpty();
	}
	
	private void setGrammarDataClient() {
		getClientDataControll().getTextFieldTerminals().setText(grammar.getTerminalSimbols());
		getClientDataControll().getTextFieldNotTerminals().setText(grammar.getNonTerminalSimbols());
		getClientDataControll().getTextAreaGrammar().setText(grammar.getGrammarStructure());
	}

	private void getGrammarDataClient() {
		grammar.setTerminalSimbols(getClientDataControll().getTextFieldTerminals().getText());
		grammar.setNonTerminalSimbols(getClientDataControll().getTextFieldNotTerminals().getText());
		grammar.setGrammarStructure(getClientDataControll().getTextAreaGrammar().getText());
	}

	public void selectStatePanelAFDAutomata(State estado) {
		getClientDataControll().getPanelViewAutomata().setSelectedState(estado);
		getClientDataControll().getPanelViewAutomata().repaint();
	}
	
	public void selectStatePanelAFNDAutomata(State estado) {
		getClientDataControll().getPanelViewAutomataGrammar().setSelectedState(estado);
		getClientDataControll().getPanelViewAutomataGrammar().repaint();
	}
	
	public void validateSentenceAction() {
		
		if(ValidationsUtils.isStringNullOrEmpty(getClientDataControll().getSentence())){
			printMessageDialog("Informe uma sentença para validar!");
			return;
		}
		validateSentenceMBean.setStatesListAutomataValidation(statesListAutomataValidation);
		validateSentenceMBean.validateSentence();
	}
	
	private void clearPaneAutomataAFD() {
		getClientDataControll().getPanelViewAutomata().setSelectedState(null);
		getClientDataControll().getPanelViewAutomata().setStates(new ArrayList<State>());
		getClientDataControll().getPanelViewAutomata().removeAll();
		getClientDataControll().getPanelViewAutomata().repaint();
	}
	
	private void clearPaneAutomataAFND() {
		getClientDataControll().getPanelViewAutomataGrammar().setSelectedState(null);
		getClientDataControll().getPanelViewAutomataGrammar().setStates(new ArrayList<State>());
		getClientDataControll().getPanelViewAutomataGrammar().removeAll();
		getClientDataControll().getPanelViewAutomataGrammar().repaint();
	}

	public TransitionTableInformation getTransitionTableInformation() {
		return transitionTableInformation;
	}

	public void setTransitionTableInformation(TransitionTableInformation transitionTableInformation) {
		this.transitionTableInformation = transitionTableInformation;
	}

	public DefaultTableModel getTableModelTransitionTableGrammar() {
		return tableModelTransitionTableGrammar;
	}

	public DefaultTableModel getTableModelTransitionTableAFD() {
		return tableModelTransitionTableAFD;
	}

	public DefaultTableModel getTableModelTransitionTableAFDSimplified() {
		return tableModelTransitionTableAFDSimplified;
	}

	public List<State> getStates() {
		return statesListAutomataValidation;
	}

	public void setGrammar(Grammar grammar) {
		this.grammar = grammar;
	}
	
	public void notifyGrammarAltered() {
		updateApplicationStage(EnumApplicationStage.GRAMMAR_INVALID);
	}

	private String aboutVirtualRegularLanguage() {
		return new StringBuilder()
				.append(ApplicationResources.getApplicationName())
				.append("\n")
				.append("Universidade Comunitária da Região de Chapecó (Unochapecó)\n")
				.append("Desenvolvido por: Luan Felipe da Silva Knebel\n")
				.append("Chapecó, Santa Catarina - Brasil\n")
				.append("Versão do Java:")
				.append(ApplicationResources.getJavaVersion())
				.append("\nData: 17/01/2017").toString();
	}
	
	private void updateApplicationStage(EnumApplicationStage applicationStage) {
		applicationStageControll.setApplicationStage(applicationStage);
	}
	
	private void updateAFNDApplicationState(Boolean AFND) {
		applicationStageControll.setAFND(AFND);
	}
	
	@Override
	public void setClientDataControll(IClientDataControll clientDataControll) {
		super.setClientDataControll(clientDataControll);
		grammarMBean = new GrammarMBean(getClientDataControll());
		fileMBean = new FileMBean(getClientDataControll());
		validateSentenceMBean = new ValidateSentenceMBean(getClientDataControll());
		tableMBean = new TransitionTableMBean(getClientDataControll());
		transformationAFNDToAFDMBean = new TransformationAFNDToAFDMBean(getClientDataControll());
	}
	
	@Override
	public void setClientButtonControll(IClientButtonControll clientButtonControll) {
		super.setClientButtonControll(clientButtonControll);
		applicationStageControll = new ApplicationStageControll(getClientButtonControll());
		validateSentenceMBean.setClientButtonControll(getClientButtonControll());
		updateApplicationStage(EnumApplicationStage.GRAMMAR_INVALID);
	}
	
	private void clearClient() {
		setGrammarDataClient();
		clearPaneAutomataAFND();
		clearPaneAutomataAFD();
		
		tableModelTransitionTableGrammar = new DefaultTableModel();
		tableModelTransitionTableAFD = new DefaultTableModel();
		tableModelTransitionTableAFDSimplified = new DefaultTableModel();
		transitionTableInformation = new TransitionTableInformation();
		
		getClientDataControll().getTextAreaConsoleValidations().setText("");
		getClientDataControll().getGrammarTable().setModel(new DefaultTableModel());
		getClientDataControll().getAFDTable().setModel(new DefaultTableModel());
		getClientDataControll().getAFDSimplifiedTable().setModel(new DefaultTableModel());
		getClientDataControll().setInformationsModel();
		getClientDataControll().getEditorPanelGramar().setText("");

	}

}
