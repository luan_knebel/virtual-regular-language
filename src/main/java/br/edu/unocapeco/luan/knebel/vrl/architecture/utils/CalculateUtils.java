package br.edu.unocapeco.luan.knebel.vrl.architecture.utils;

public class CalculateUtils {

    public static double generateHipotenuse(
            double x1, double y1,
            double x2, double y2 ) {

        double x = Math.abs( x1 ) - Math.abs( x2 );
        double y = Math.abs( y1 ) - Math.abs( y2 );

        return Math.sqrt( Math.pow( x, 2 ) + Math.pow( y, 2 ) );

    }

    private static int detectQuadrant(
            double x1, double y1,
            double x2, double y2 ) {

        if ( ( x2 > x1 && y2 > y1 ) ||
                ( x2 > x1 && y2 == y1 ) ||
                ( x2 == x1 && y2 > y1 ) ||
                ( x2 == x1 && y2 == y1 ) )
            return 1;

        if ( ( x2 < x1 && y2 > y1 ) )
            return 2;

        if ( ( x2 < x1 && y2 < y1 ) ||
                ( x2 == x1 && y2 < y1 ) ||
                ( x2 < x1 && y2 == y1 ) )
            return 3;
        
        return 4;

    }

    private static int generateAngleIngrement(
            double x1, double y1,
            double x2, double y2 ) {

        int q = detectQuadrant( x1, y1, x2, y2 );

        if ( q == 1 ) {
            return 0;
        } else if ( q == 2 ) {
            return 90;
        } else if ( q == 3 ) {
            return 180;
        } else {
            return 270;
        }

    }

    public static double getRelativeRate(
            double x1, double y1,
            double x2, double y2 ) {

        double x = 0;
        double y = 0;

        if ( x2 > x1 )
            x = x2 - x1;
        else
            x = x1 - x2;

        if ( y2 > y1 )
            y = y2 - y1;
        else
            y = y1 - y2;

        return generateAngleIngrement( x1, y1, x2, y2 ) +
                Math.toDegrees( Math.atan2( y, x ) );

    }

    public static double getJavaRelativeRate(
            double x1, double y1,
            double x2, double y2 ) {

        double x = 0;
        double y = 0;

        if ( x2 > x1 )
            x = x2 - x1;
        else
            x = x1 - x2;

        if ( y2 > y1 )
            y = y2 - y1;
        else
            y = y1 - y2;

        int incr = generateAngleIngrement( x1, y1, x2, y2 );
        double ang = Math.toDegrees( Math.atan2( y, x ) );

        if ( incr == 90 || incr == 270 )
            ang = 90 - ang;

        return incr + ang;

    }


}
