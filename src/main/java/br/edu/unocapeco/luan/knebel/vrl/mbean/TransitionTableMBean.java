package br.edu.unocapeco.luan.knebel.vrl.mbean;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.swing.table.DefaultTableModel;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.ValidationsUtils;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.beans.Transition;
import br.edu.unocapeco.luan.knebel.vrl.beans.TransitionTableInformation;

public class TransitionTableMBean extends AbstractClientControll {

	private List<State> statesList = new ArrayList<State>();
	private TransitionTableInformation transitionTableInformation;

	public TransitionTableMBean(IClientDataControll clientControll) {
		super(clientControll);
	}

	public DefaultTableModel createTableModelTransitions() {
		if(ValidationsUtils.isObjectNull(statesList)) return null;
		
		transitionTableInformation = new TransitionTableInformation();
		StringBuilder builderStates = new StringBuilder();
		StringBuilder builderAphabet = new StringBuilder();
		StringBuilder builderFinalStates = new StringBuilder();

		TreeSet<Character> setSimbols = new TreeSet<Character>();

		for (State estates : statesList) {

			builderStates.append(estates.getName());
			builderStates.append(", ");

			if (estates.isFinal()) {
				builderFinalStates.append(estates.getName());
				builderFinalStates.append(", ");
			}

			for (Transition transition : estates.getTransitions()) {
				for (Character simbols : transition.getSimbols()) {
					setSimbols.add(simbols);
				}
			}
		}

		if (builderStates.toString().length() > 2) {
			transitionTableInformation.setStates(
					"Q = { " + builderStates.toString().substring(0, builderStates.toString().length() - 2) + " }");
		}

		if (builderFinalStates.toString().length() > 2) {
			transitionTableInformation.setFinalStates("F = { "
					+ builderFinalStates.toString().substring(0, builderFinalStates.toString().length() - 2) + " }");
		}

		for (Character letter : setSimbols) {
			builderAphabet.append(letter);
			builderAphabet.append(", ");
		}

		if (builderAphabet.toString().length() > 2) {
			transitionTableInformation.setAlphabet("\u03a3 = { "
					+ builderAphabet.toString().substring(0, builderAphabet.toString().length() - 2) + " }");
		}

		return createTableModel(setSimbols, statesList);

	}

	private DefaultTableModel createTableModel(TreeSet<Character> set, List<State> estado) {

		Object[] titulos = new Object[set.size() + 1];
		titulos[0] = "\u03b4";
		int i = 1;

		for (Character character : set) {
			titulos[i++] = character;
		}

		DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
			private static final long serialVersionUID = -1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		i = 0;

		for (State state : statesList) {
			
			modelo.addRow(new Object[set.size() + 1]);
			modelo.setValueAt(state, i, 0);

			int j = 1;

			for (Character character : set) {
				for (Transition transition : state.getTransitions()) {
					for (Character simbol : transition.getSimbols()) {
						if (simbol == Character.valueOf(character)) {
							if(ValidationsUtils.isObjectNull(modelo.getValueAt(i, j))){
								modelo.setValueAt(transition.getDestinyState().getName(), i, j);
							}else{
								modelo.setValueAt(modelo.getValueAt(i, j) + "," + transition.getDestinyState().getName(), i, j);
							}
						}
					}
				}
				j++;
			}
			i++;
		}

		return modelo;
	}

	public List<State> getStatesList() {
		return statesList;
	}

	public void setStatesList(List<State> statesList) {
		this.statesList = statesList;
	}

	public TransitionTableInformation getTransitionTableInformation() {
		return transitionTableInformation;
	}

	public void setTransitionTableInformation(TransitionTableInformation transitionTableInformation) {
		this.transitionTableInformation = transitionTableInformation;
	}

}
