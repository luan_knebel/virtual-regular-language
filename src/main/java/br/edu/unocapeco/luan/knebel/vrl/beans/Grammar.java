package br.edu.unocapeco.luan.knebel.vrl.beans;

public class Grammar {
	
	private String terminalSimbols;
	private String nonTerminalSimbols;
	private String grammarStructure;
	private boolean AFND;
	
	public Grammar() {}
	
	public Grammar(String terminalSimbols, String nonTerminalSimbols, String grammarStructure) {
		super();
		this.terminalSimbols = terminalSimbols;
		this.nonTerminalSimbols = nonTerminalSimbols;
		this.grammarStructure = grammarStructure;
	}
	
	public String getTerminalSimbols() {
		return terminalSimbols;
	}
	
	public void setTerminalSimbols(String terminalSimbols) {
		this.terminalSimbols = terminalSimbols;
	}
	
	public String getNonTerminalSimbols() {
		return nonTerminalSimbols;
	}
	
	public void setNonTerminalSimbols(String nonTerminalSimbols) {
		this.nonTerminalSimbols = nonTerminalSimbols;
	}
	
	public String getGrammarStructure() {
		return grammarStructure;
	}
	
	public void setGrammarStructure(String grammarStructure) {
		this.grammarStructure = grammarStructure;
	}

	public boolean isAFND() {
		return AFND;
	}

	public void setAFND(boolean aFND) {
		AFND = aFND;
	}
	
}
