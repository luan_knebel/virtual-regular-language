package br.edu.unocapeco.luan.knebel.vrl.architecture.client;

import javax.swing.JFrame;

import br.edu.unocapeco.luan.knebel.vrl.architecture.resources.ApplicationResources;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;

public abstract class AbstractFrame extends JFrame implements IClientDataControll, IClientButtonControll {

	private static final long serialVersionUID = 1L;
	
	private VirtualRegularLanguageMBean mBean;
	
	public AbstractFrame() {
		setTitle(ApplicationResources.getApplicationName());
		setIconImage(ApplicationResources.getApplicationLogo());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocationRelativeTo(null);
		this.mBean = new VirtualRegularLanguageMBean();
	}
	
	public VirtualRegularLanguageMBean getManageBean() {
		return mBean;
	}
	
}
