package br.edu.unocapeco.luan.knebel.vrl.beans;

public class TransitionTableInformation {
	
    private String alphabet;
    private String automata;
    private String states;
    private String finalStates;
    private String delta;
    
    public TransitionTableInformation() {
    	alphabet = "\u03a3 = \u00d8 ";
    	automata = "M = (Q, \u03a3, \u03b4, q0, F)";
    	states = "Q = \u00d8 ";
    	finalStates = "F = \u00d8 ";
    	delta = "\u03b4: ";
	}
    
	public String getAlphabet() {
		return alphabet;
	}
	public void setAlphabet(String alphabet) {
		this.alphabet = alphabet;
	}
	public String getAutomata() {
		return automata;
	}
	public void setAutomata(String automata) {
		this.automata = automata;
	}
	public String getDelta() {
		return delta;
	}
	public void setDelta(String delta) {
		this.delta = delta;
	}
	public String getStates() {
		return states;
	}
	public void setStates(String states) {
		this.states = states;
	}
	public String getFinalStates() {
		return finalStates;
	}
	public void setFinalStates(String finalStates) {
		this.finalStates = finalStates;
	}
    
	
}
