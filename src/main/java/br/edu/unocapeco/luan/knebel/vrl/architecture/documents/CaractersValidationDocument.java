package br.edu.unocapeco.luan.knebel.vrl.architecture.documents;

import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.CharacterUtils;

public class CaractersValidationDocument {

	public static String[] getValidCaractersTerminals() {
		return CharacterUtils.getValidCharacterGrammarMetaDataTerminals();
	}
	
	public static String[] getValidCaractersNonTerminals() {
		return CharacterUtils.getValidCharacterGrammarMetaDataNonTerminals();
	}
	
}
