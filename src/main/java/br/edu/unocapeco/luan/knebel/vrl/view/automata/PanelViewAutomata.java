
package br.edu.unocapeco.luan.knebel.vrl.view.automata;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.CalculateUtils;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.CharacterUtils;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.beans.Transition;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;

public class PanelViewAutomata extends AbstractPanel {
	
	private static final long serialVersionUID = 1L;
	
	private List< State > states;
    private State selectedState;
    
    public PanelViewAutomata(VirtualRegularLanguageMBean mBean) {
    	super(mBean);
        initComponents();
        states = new ArrayList<State>();
    }

    private void initComponents() {
    	setBackground( Color.WHITE );
        setLayout(null);
    }

    @Override
    protected void paintComponent( Graphics g ) {
        super.paintComponent( g );
        
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        g2d.setPaint( Color.BLACK );
        FontMetrics fm = g2d.getFontMetrics();
        
        g2d.draw(new Rectangle2D.Double(0, 0, getWidth() - 1, getHeight() - 1));
        
        if ( selectedState != null ) {
            g2d.setPaint(Color.ORANGE);
            g2d.fill( new Ellipse2D.Double(selectedState.getXCentral() - 29, selectedState.getYCentral() - 29, 58, 58));
            
        }
        
        g2d.setPaint( Color.BLACK );
        
        for (State state : states) {
        	
            for (Transition transition : state.getTransitions()) {
                
                if (!transition.getDestinyState().equals( state )) {

                    g2d.draw( new Line2D.Double(
                            state.getXCentral(), state.getYCentral(), 
                            transition.getDestinyState().getXCentral(), 
                            transition.getDestinyState().getYCentral() ) );
                    
                    String simbols;
                    if(existInverseTransition(state, transition)){
                    	simbols = getAllSymbolsForTransitions(state, transition.getDestinyState());
                    }else{
                    	simbols = transition.generatedStringSimbols();
                    }
                    
                    int sockLargSimbols = fm.stringWidth(simbols) / 2;
                    
                    g2d.drawString(simbols,
                            state.getXCentral() + ((transition.getDestinyState().getXCentral() - state.getXCentral() ) / 2) - sockLargSimbols,
                            state.getYCentral() + ((transition.getDestinyState().getYCentral() - state.getYCentral() ) / 2) - 5);
                    
                    double h = CalculateUtils.generateHipotenuse(
                            state.getXCentral(), state.getYCentral(),
                            transition.getDestinyState().getXCentral(),
                            transition.getDestinyState().getYCentral() );

                    double gr = CalculateUtils.getJavaRelativeRate(
                            state.getXCentral(), state.getYCentral(),
                            transition.getDestinyState().getXCentral(),
                            transition.getDestinyState().getYCentral());

                    double x = (h - 25 ) * Math.cos(Math.toRadians(gr));
                    double y = (h - 25 ) * Math.sin(Math.toRadians(gr));
                    
                    Graphics2D g2df = (Graphics2D) g2d.create();

                    g2df.translate(x + state.getXCentral(), y + state.getYCentral());
                    g2df.rotate(Math.toRadians(gr));
                    g2df.draw(new Line2D.Double( 0, 0, -5, -5 ));
                    g2df.draw(new Line2D.Double( 0, 0, -5, 5 ));
                    g2df.dispose();

                } else { 
                    
                    g2d.draw(new Ellipse2D.Double(
                            state.getXCentral(), state.getYCentral() - 40, 30, 30));
                    
                    g2d.draw(new Line2D.Double(
                            state.getXCentral() + 21, state.getYCentral() - 11, 
                            state.getXCentral() + 30, state.getYCentral() - 11));
                    
                    g2d.draw(new Line2D.Double(
                            state.getXCentral() + 22, state.getYCentral() - 11, 
                            state.getXCentral() + 21, state.getYCentral() - 20));
                    
                    g2d.drawString( transition.generatedStringSimbols(),
                            state.getXCentral() + 15,
                            state.getYCentral() - 45);
                    
                }
            }
        }
    }
    
    private boolean existInverseTransition(State currentState, Transition currentTransition) {
    	
    	for (Transition transition : currentTransition.getDestinyState().getTransitions()) {
    		if(transition.getDestinyState().equals(currentState)) return true;
		}
    	return  false;
	}
    
    
    private String getAllSymbolsForTransitions(State stateOrigin, State stateDestiny) {
    	String simbols = "";
    	
    	State stateInitialTransition;
    	State stateFinalTransition;
    	Integer alphabetNumberStateOrigin = CharacterUtils.getNumberByAphabetSimbol(stateOrigin.getName());
    	Integer alphabetNumberStateDestiny = CharacterUtils.getNumberByAphabetSimbol(stateDestiny.getName());
    	
    	if(alphabetNumberStateOrigin > alphabetNumberStateDestiny){
    		stateInitialTransition = stateOrigin;
    		stateFinalTransition = stateDestiny;
    	}else{
    		stateInitialTransition = stateDestiny;
    		stateFinalTransition = stateOrigin;
    	}
    	
    	for (Transition transition : stateFinalTransition.getTransitions()) {
    		if(transition.getDestinyState().equals(stateInitialTransition)){
    			simbols = transition.generatedStringSimbols();
    		}
		}
    	
    	for (Transition transition : stateInitialTransition.getTransitions()) {
    		if(transition.getDestinyState().equals(stateFinalTransition)){
    			simbols += " \\ " + transition.generatedStringSimbols();
    		}
		}
    	
    	return simbols;
	}
    
    
    public void setStates(List<State> states) {
        this.states = states;
    }
    
    public void setSelectedState(State selectedState) {
        this.selectedState = selectedState;
    }
    
}
