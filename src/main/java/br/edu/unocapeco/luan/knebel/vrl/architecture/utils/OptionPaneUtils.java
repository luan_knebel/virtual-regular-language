package br.edu.unocapeco.luan.knebel.vrl.architecture.utils;

import java.awt.Component;

import javax.swing.JOptionPane;

public class OptionPaneUtils {

	public static void printMessageDialog(Component parent, String message) {
		printMessageDialog(parent, message, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void printMessageDialog(Component parent, String message, int messageType) {
		JOptionPane.showMessageDialog(parent, message, "Informação", messageType);
	}
	
}
