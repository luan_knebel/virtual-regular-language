package br.edu.unocapeco.luan.knebel.vrl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientButtonControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;
import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelViewAutomata;

public class PanelVirtualRegularLanguage extends AbstractPanel implements IClientDataControll, IClientButtonControll{

	private static final long serialVersionUID = 1L;

	private JTabbedPane tabbedPane = new JTabbedPane();
	private PanelGrammar tabPanelGrammar;
	private PanelTransitionTable tabPanelTransitionTable;
	private PanelTransformationAFND tabTransformationAFND;
	private PanelAutomata tabPanelAutomata;
	
	private Dimension dimensionTabs =new Dimension(205, 15);
	private JLabel labelTabGrammar = new JLabel();
	private JLabel labelTabTransitionTable = new JLabel();
	private JLabel labelTabTransformationAFND = new JLabel();
	private JLabel labelTabAutomata = new JLabel();
	
	public PanelVirtualRegularLanguage(VirtualRegularLanguageMBean mBean) {
		super(mBean);
		setLayout(new BorderLayout());
		initComponents();
	}
	
	private void initComponents() {
		tabPanelGrammar = new PanelGrammar(getManageBean());
		tabPanelTransitionTable = new PanelTransitionTable(getManageBean());
		tabTransformationAFND = new PanelTransformationAFND(getManageBean());
		tabPanelAutomata = new PanelAutomata(getManageBean());
		
		Border borderPanel = BorderFactory.createEmptyBorder(20, 20, 20, 20);

		labelTabTransitionTable.setLayout(new GridLayout(1, 2));
		labelTabTransitionTable.setBorder(borderPanel);
		
		labelTabAutomata.setLayout(new GridLayout(1, 2));
		labelTabAutomata.setBorder(borderPanel);
		
		tabbedPane.addTab(null, tabPanelGrammar);
		tabbedPane.addTab(null, tabPanelTransitionTable);
		tabbedPane.addTab(null, tabTransformationAFND);
		tabbedPane.addTab(null, tabPanelAutomata);
		
		labelTabGrammar.setText("Gramática Regular");
		labelTabGrammar.setPreferredSize(dimensionTabs);
		labelTabGrammar.setHorizontalAlignment(SwingConstants.CENTER);
		tabbedPane.setTabComponentAt(0, labelTabGrammar);

		labelTabTransitionTable.setText("Tabela de Transição");
		labelTabTransitionTable.setPreferredSize(dimensionTabs);
		labelTabTransitionTable.setHorizontalAlignment(SwingConstants.CENTER);
		tabbedPane.setTabComponentAt(1, labelTabTransitionTable);
		
		labelTabTransformationAFND.setText("Transformação AFND/AFD");
		labelTabTransformationAFND.setPreferredSize(dimensionTabs);
		labelTabTransformationAFND.setHorizontalAlignment(SwingConstants.CENTER);
		tabbedPane.setTabComponentAt(2, labelTabTransformationAFND);
		
		labelTabAutomata.setText("Validação de Sentença");
		labelTabAutomata.setPreferredSize(dimensionTabs);
		labelTabAutomata.setHorizontalAlignment(SwingConstants.CENTER);
		tabbedPane.setTabComponentAt(3, labelTabAutomata);
		
		add(tabbedPane, BorderLayout.CENTER);
	}
	
	public void resetTabbedPane() {
		tabbedPane.setSelectedIndex(0);
	}

	@Override
	public PanelViewAutomata getPanelViewAutomata() {
		return tabPanelAutomata.getPanelViewAutomata();
	}
	
	@Override
	public JTextField getTextFieldTerminals() {
		return tabPanelGrammar.getTextFieldTerminals();
	}

	@Override
	public JTextField getTextFieldNotTerminals() {
		return tabPanelGrammar.getTextFieldNotTerminals();
	}

	@Override
	public JTextArea getTextAreaGrammar() {
		return tabPanelGrammar.getTextAreaGrammar();
	}
	
	@Override
	public JTextArea getTextAreaConsoleValidations() {
		return tabPanelAutomata.getTextAreaConsoleValidations();
	}
	
	@Override
	public String getSentence() {
		return tabPanelAutomata.getSentence();
	}
	
	@Override
	public PanelViewAutomata getPanelViewAutomataGrammar() {
		return tabPanelTransitionTable.getPanelViewAutomataGrammar();
	}
	
	@Override
	public JTable getGrammarTable() {
		return tabPanelTransitionTable.getGrammarTable();
	}
	
	@Override
	public JTable getAFDTable() {
		return tabTransformationAFND.getAFDTable();
	}
	
	@Override
	public JTable getAFDSimplifiedTable() {
		return tabTransformationAFND.getAFDSimplifiedTable();
	}
	
	@Override
	public void setInformationsModel(){
		tabPanelAutomata.setInformationsModel();
	}
	
	@Override
	public JEditorPane getEditorPanelGramar() {
		return tabPanelGrammar.getEditorPanelGramar();
	}

	@Override
	public void setButtonValidateGrammarEnabled(Boolean enabled) {
		tabPanelGrammar.setButtonValidateGrammarEnabled(enabled); 
	}

	@Override
	public void setButtonCreateGrammarModifiedEnabled(Boolean enabled) {
		tabPanelGrammar.setButtonCreateGrammarModifiedEnabled(enabled); 
	}

	@Override
	public void setButtonGenerateTableEnabled(Boolean enabled) {
		tabPanelTransitionTable.setButtonGenerateTableEnabled(enabled);
	}

	@Override
	public void setButtonGenerateAutomataTableEnabled(Boolean enabled) {
		tabPanelTransitionTable.setButtonGenerateAutomataTableEnabled(enabled);
	}

	@Override
	public void setButtonTableAFNDEnabled(Boolean enabled) {
		tabTransformationAFND.setButtonTableAFNDEnabled(enabled);
	}

	@Override
	public void setButtonTableAFNDSimplifiedEnabled(Boolean enabled) {
		tabTransformationAFND.setButtonTableAFNDSimplifiedEnabled(enabled);
	}

	@Override
	public void setButtonGenerateAutomataValidateEnabled(Boolean enabled) {
		tabPanelAutomata.setButtonGenerateAutomataValidateEnabled(enabled);
	}

	@Override
	public void setButtonValidateSentencesEnabled(Boolean enabled) {
		tabPanelAutomata.setButtonValidateSentencesEnabled(enabled);
	}

	@Override
	public Component getClientParent() {
		return getParent();
	}
	
}



