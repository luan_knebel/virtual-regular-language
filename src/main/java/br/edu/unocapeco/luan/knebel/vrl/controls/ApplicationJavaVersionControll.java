package br.edu.unocapeco.luan.knebel.vrl.controls;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

import br.edu.unocapeco.luan.knebel.vrl.architecture.resources.ApplicationResources;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.OptionPaneUtils;

public class ApplicationJavaVersionControll {

	public static void validateJavaVersion() {
		if(!isValidJavaVersion()){
			int returnQuestionValue = JOptionPane.showConfirmDialog(null, getMessage(),"Versão do Java Incompatível",JOptionPane.YES_NO_OPTION);
			if(returnQuestionValue == JOptionPane.YES_OPTION){
				openJavaUpdateBrowser();
			}
			System.exit(0);
		}
	}
	
	private static String getMessage() {
		return new StringBuilder()
				.append("Este aplicativo requer o Java 1.8 ou superior.\n")
				.append("Versão Java detectada: ")
				.append(ApplicationResources.getJavaVersion())
				.append("\nDeseja atualizar o Java em seu computador?").toString();
	}
	
	private static void openJavaUpdateBrowser() {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI("https://java.com/pt_BR/download"));
			} catch (IOException | URISyntaxException e) {
				OptionPaneUtils.printMessageDialog(null, "Não foi possível abrir o site de download do Java");
				e.printStackTrace();
			} 
		}
	}
	
	public static boolean isValidJavaVersion() {
		String javaVersionJDK = ApplicationResources.getJavaVersion().substring(0, 3);
		Double javaVersion = Double.valueOf(javaVersionJDK);
		return (javaVersion >= 1.8);
	}
	
}
