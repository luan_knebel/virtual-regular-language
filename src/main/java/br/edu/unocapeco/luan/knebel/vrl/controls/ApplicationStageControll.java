package br.edu.unocapeco.luan.knebel.vrl.controls;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientButtonControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumApplicationStage;

public class ApplicationStageControll extends AbstractClientControll {

	private EnumApplicationStage applicationStage;
	
	private boolean AFND;
	
	public ApplicationStageControll(IClientButtonControll clientButtonControll) {
		setClientButtonControll(clientButtonControll);
	}
	
	public EnumApplicationStage getApplicationStage() {
		return applicationStage;
	}

	public void setApplicationStage(EnumApplicationStage applicationStage) {
		this.applicationStage = applicationStage;
		updateButtons();
	}

	public boolean isAFND() {
		return AFND;
	}

	public void setAFND(boolean aFND) {
		this.AFND = aFND;
	}
	
	private void updateButtons() {
		getClientButtonControll().setButtonCreateGrammarModifiedEnabled(getButtonCreateGrammarModifiedEnabled());
		getClientButtonControll().setButtonGenerateTableEnabled(getButtonGenerateTableEnabled());
		getClientButtonControll().setButtonGenerateAutomataTableEnabled(getButtonGenerateAutomataTableEnabled());
		getClientButtonControll().setButtonTableAFNDEnabled(getButtonTableAFNDEnabled());
		getClientButtonControll().setButtonTableAFNDSimplifiedEnabled(getButtonTableAFNDSimplifiedEnabled());
		getClientButtonControll().setButtonGenerateAutomataValidateEnabled(getButtonGenerateAutomataValidateEnabled());
		getClientButtonControll().setButtonValidateSentencesEnabled(getButtonValidateSentencesEnabled());
	}
	
	private Boolean getButtonValidateSentencesEnabled() {
		return isAutomataAFDGenerated();
	}

	private Boolean getButtonGenerateAutomataValidateEnabled() {
		return ((isTableAFDSimplifiedGenerated() && isAFND()) || (isTableGrammarGenerated() && !isAFND()));
	}

	private Boolean getButtonTableAFNDSimplifiedEnabled() {
		return isTableAFDGenerated();
	}

	private Boolean getButtonTableAFNDEnabled() {
		return isAutomataAFNDGenerated();
	}

	private boolean getButtonGenerateAutomataTableEnabled() {
		return isTableGrammarGenerated() && isAFND();
	}
	
	private boolean getButtonCreateGrammarModifiedEnabled(){
		return isValidGrammar();
	}
	
	private boolean getButtonGenerateTableEnabled() {
		if(isInvalidGrammar()) return false;
		return (isGrammarModified());
	}
	
	private boolean isInvalidGrammar() {
		return applicationStage == EnumApplicationStage.GRAMMAR_INVALID;
	}

	private boolean isValidGrammar() {
		return applicationStage == EnumApplicationStage.GRAMMAR_VALID;
	}
	
	private boolean isGrammarModified() {
		return applicationStage == EnumApplicationStage.GRAMMAR_MODIFIED;
	}
	
	private boolean isTableGrammarGenerated() {
		return applicationStage == EnumApplicationStage.TABLE_GRAMMAR_GENERATED;
	}
	
	private boolean isAutomataAFNDGenerated() {
		return applicationStage == EnumApplicationStage.AUTOMATA_AFND_GENERATED;
	}
	
	private boolean isTableAFDGenerated() {
		return applicationStage == EnumApplicationStage.TABLE_AFD_GENERATED;
	}
	
	private boolean isTableAFDSimplifiedGenerated() {
		return applicationStage == EnumApplicationStage.TABLE_AFD_SIMPLIFIED_GENERATED;
	}
	
	private boolean isAutomataAFDGenerated() {
		return applicationStage == EnumApplicationStage.AUTOMATA_AFD_GENERATED;
	}
	
	
}
