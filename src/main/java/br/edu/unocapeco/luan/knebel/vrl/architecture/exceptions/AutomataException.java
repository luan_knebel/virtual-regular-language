package br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions;

import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumAutomataExceptionMessage;

public class AutomataException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public AutomataException(EnumAutomataExceptionMessage message) {
		super(message.getMessage());
	}
	
	public AutomataException(EnumAutomataExceptionMessage message, Throwable cause) {
		super(message.getMessage(), cause);
	}
	
	public AutomataException(Throwable cause) {
		super(cause);
	}
	

}
