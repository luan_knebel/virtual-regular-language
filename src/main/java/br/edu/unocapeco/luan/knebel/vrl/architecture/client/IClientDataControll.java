package br.edu.unocapeco.luan.knebel.vrl.architecture.client;

import java.awt.Component;

import javax.swing.JEditorPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelViewAutomata;

public interface IClientDataControll{
	
	public PanelViewAutomata getPanelViewAutomata();
	
	public PanelViewAutomata getPanelViewAutomataGrammar();
	
	public JTextField getTextFieldTerminals();
	
	public JTextField getTextFieldNotTerminals();
	
	public JTextArea getTextAreaGrammar();
	
	public JTextArea getTextAreaConsoleValidations();
	
	public String getSentence();
	
	public JTable getGrammarTable();
	
	public JTable getAFDTable();
	
	public JTable getAFDSimplifiedTable();
	
	public Component getClientParent();
	
	public void setInformationsModel();
	
	public JEditorPane getEditorPanelGramar();
	
}
