package br.edu.unocapeco.luan.knebel.vrl.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.architecture.documents.LowerCaseDocument;
import br.edu.unocapeco.luan.knebel.vrl.beans.TransitionTableInformation;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;
import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelViewAutomata;
import br.edu.unocapeco.luan.knebel.vrl.view.components.PanelTextFieldDescription;

public class PanelAutomata extends AbstractPanel{

	private static final long serialVersionUID = 1L;
	
	private PanelViewAutomata panelViewAutomata;
	
	private JSplitPane panelMain;
	private JPanel panelAutomata = new JPanel();
	private JPanel panelAutomataButton = new JPanel();
	private JPanel panelAutomataLabel = new JPanel();
	private JPanel panelAutomataValidations = new JPanel();
	private JPanel panelAutomataValidationsSentences = new JPanel();
	private JPanel panelAutomataValidationsSentencesButton = new JPanel();
	private JPanel panelAutomataInformations = new JPanel();
	private JPanel panelAutomataInformationsLabels = new JPanel();
	private JPanel panelButtonValidateSentences = new JPanel();
	private JPanel panelTextAreaConsoleValidation = new JPanel();
	
	private PanelTextFieldDescription panelTexfDescriptionSentence = new PanelTextFieldDescription("Sentença", 85);
	
	JScrollPane scrollPaneConsoleValidation = new JScrollPane();
	private JTextArea textAreaConsoleValidation = new JTextArea();
	
	private JButton buttonGenerateAutomata = new JButton();
	private JButton buttonValidateSentences = new JButton();
	
    private JLabel labelAphabet = new JLabel();
    private JLabel labelAutomata = new JLabel();
    private JLabel labelDelta = new JLabel();
    private JLabel labelStates = new JLabel();
    private JLabel labelFinalStates = new JLabel();
    private JLabel labelPanelAutomata = new JLabel();
    private JLabel labelPanelAutomataInformations = new JLabel();
    private JLabel labelConsoleValidations = new JLabel();
	
	public PanelAutomata(VirtualRegularLanguageMBean mBean) {
		super(mBean);
		initComponents();
	}
	
	private void initComponents() {
		setLayout(new BorderLayout());
		setBorder(getPatternEmptyBorder());
		setInformationsModel();
		panelViewAutomata = new PanelViewAutomata(getManageBean());
		panelTexfDescriptionSentence.setDocument(new LowerCaseDocument(50));
		
		labelPanelAutomata.setText("Autômato Finito AFD:");
		buttonGenerateAutomata.setText("Gerar Autômato AFD");
		buttonGenerateAutomata.setPreferredSize(new Dimension(200, 25));
		buttonGenerateAutomata.setFocusable(false);
		buttonGenerateAutomata.addActionListener(action -> createAutomataAFDAction());
		
		panelAutomataButton.setLayout(new BorderLayout());
		panelAutomataButton.add(buttonGenerateAutomata, BorderLayout.EAST);
		
		panelAutomataLabel.setLayout(new GridLayout(2, 1));
		panelAutomataLabel.add(panelAutomataButton, BorderLayout.NORTH);
		panelAutomataLabel.add(labelPanelAutomata);
		
		panelAutomata.setLayout(new BorderLayout());
		panelAutomata.add(panelAutomataLabel, BorderLayout.NORTH);
		panelAutomata.add(panelViewAutomata);
		
		labelPanelAutomataInformations.setText("Informações do Autômato:");
		
		panelAutomataInformationsLabels.setLayout(new GridLayout(4, 1));
		panelAutomataInformationsLabels.setBorder(new LineBorder(Color.BLACK));
		panelAutomataInformationsLabels.add(labelAutomata);
		panelAutomataInformationsLabels.add(labelStates);
		panelAutomataInformationsLabels.add(labelAphabet);
		panelAutomataInformationsLabels.add(labelFinalStates);
		
		panelAutomataInformations.setLayout(new BorderLayout());
		panelAutomataInformations.setBorder( BorderFactory.createEmptyBorder(0, 0, 10, 0));
		panelAutomataInformations.add(labelPanelAutomataInformations, BorderLayout.NORTH);
		panelAutomataInformations.add(panelAutomataInformationsLabels);
		
		buttonValidateSentences.setText("Validar Sentença");
		buttonValidateSentences.setPreferredSize(new Dimension(200, 25));
		buttonValidateSentences.setFocusable(false);
		buttonValidateSentences.addActionListener(action -> getManageBean().validateSentenceAction());
		panelButtonValidateSentences.setLayout(new BorderLayout());
		panelButtonValidateSentences.add(buttonValidateSentences, BorderLayout.WEST);
		
		panelTexfDescriptionSentence.setBorder( BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		panelAutomataValidationsSentencesButton.setLayout(new BorderLayout());
		panelAutomataValidationsSentencesButton.add(panelButtonValidateSentences, BorderLayout.NORTH);
		panelAutomataValidationsSentencesButton.add(panelTexfDescriptionSentence);
		
		labelConsoleValidations.setText("Passos da Validação da Sentença:");
		textAreaConsoleValidation.setEditable(false);
		scrollPaneConsoleValidation.setViewportView(textAreaConsoleValidation);
		
		panelTextAreaConsoleValidation.setLayout(new BorderLayout());
		panelTextAreaConsoleValidation.add(labelConsoleValidations, BorderLayout.NORTH);
		panelTextAreaConsoleValidation.add(scrollPaneConsoleValidation);
		
		panelAutomataValidationsSentences.setLayout(new BorderLayout());
		panelAutomataValidationsSentences.add(panelAutomataValidationsSentencesButton, BorderLayout.NORTH);
		panelAutomataValidationsSentences.add(panelTextAreaConsoleValidation);
		
		panelAutomataValidations.setLayout(new BorderLayout());
		panelAutomataValidations.add(panelAutomataInformations, BorderLayout.NORTH);
		panelAutomataValidations.add(panelAutomataValidationsSentences, BorderLayout.CENTER);
		
		panelMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		panelMain.setResizeWeight(0.7);
		panelMain.setEnabled(false);
		panelMain.setDividerSize(15);
		panelMain.add(panelAutomata);
		panelMain.add(panelAutomataValidations);
		
		add(panelMain, BorderLayout.CENTER);
	}

	private void createAutomataAFDAction() {
		getManageBean().createAutomataAFDAction();
		setInformationsModel();
	}
	
	public PanelViewAutomata getPanelViewAutomata() {
		return panelViewAutomata;
	}

	public JTextArea getTextAreaConsoleValidations() {
		return textAreaConsoleValidation;
	}
	
	public String getSentence() {
		return panelTexfDescriptionSentence.getText();
	}

	public void setInformationsModel(){
		TransitionTableInformation informationsModel = getManageBean().getTransitionTableInformation();
		labelAphabet.setText(informationsModel.getAlphabet());
		labelAutomata.setText(informationsModel.getAutomata());
		labelDelta.setText(informationsModel.getDelta());
		labelStates.setText(informationsModel.getStates());
		labelFinalStates.setText(informationsModel.getFinalStates());
	}

	public void setButtonGenerateAutomataValidateEnabled(Boolean enabled) {
		buttonGenerateAutomata.setEnabled(enabled);
	}

	public void setButtonValidateSentencesEnabled(Boolean enabled) {
		buttonValidateSentences.setEnabled(enabled);
	}
	
}
