package br.edu.unocapeco.luan.knebel.vrl.architecture.client;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;

public abstract class AbstractPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private VirtualRegularLanguageMBean mBean;
	
	public AbstractPanel(VirtualRegularLanguageMBean mBean) {
		this.mBean = mBean;
	}
	
	public VirtualRegularLanguageMBean getManageBean() {
		return mBean;
	}

	public Border getPatternEmptyBorder(){
		return BorderFactory.createEmptyBorder(20, 20, 20, 20);
	}
	
	
}
