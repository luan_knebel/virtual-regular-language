package br.edu.unocapeco.luan.knebel.vrl.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;

public class PanelTransformationAFND extends AbstractPanel{

	private static final long serialVersionUID = 1L;
	
	public PanelTransformationAFND(VirtualRegularLanguageMBean mBean) {
		super(mBean);
		initComponets();
	}
	
	private JSplitPane panelMain;
	private JPanel panelTableAFND =  new JPanel();
	private JPanel panelTableAFNDTop =  new JPanel();
	private JPanel panelTableAFNDButton =  new JPanel();
	
	private JPanel panelTableAFNDSimplified =  new JPanel();
	private JPanel panelTableAFNDSimplifiedTop =  new JPanel();
	private JPanel panelTableAFNDSimplifiedButton =  new JPanel();
	
	private JLabel labelTableAFND = new JLabel();
	private JLabel labelTableAFNDSimplified = new JLabel();
	
	private JButton buttonTableAFND = new JButton();
	private JButton buttonTableAFNDSimplified = new JButton();
	
	private JTable tableAFDTransitions = new JTable();
	private JTable tableAFDTransitionsSimplified = new JTable();
	
	private JScrollPane scrollTableAFND = new JScrollPane();
	private JScrollPane scrollTableAFNDSimplified = new JScrollPane();
	
	private void initComponets() {
		setLayout(new GridLayout(1, 2));
		setBorder(getPatternEmptyBorder());
		
		buttonTableAFND.setText("Transformar AFND para AFD");
		buttonTableAFND.addActionListener(action -> createAFDTableAction());
		buttonTableAFND.setFocusable(false);
		buttonTableAFND.setPreferredSize(new Dimension(250, 25));
		
		panelTableAFNDButton.setLayout(new BorderLayout());
		panelTableAFNDButton.add(buttonTableAFND, BorderLayout.EAST);
		
		labelTableAFND.setText("Tabela AFD:");
		
		panelTableAFNDTop.setLayout(new GridLayout(2, 1));
		panelTableAFNDTop.add(panelTableAFNDButton, BorderLayout.NORTH);
		panelTableAFNDTop.add(labelTableAFND);
		
		tableAFDTransitions.getTableHeader().setReorderingAllowed(false);
		scrollTableAFND.setViewportView(tableAFDTransitions);
		scrollTableAFND.setPreferredSize(new Dimension(200, 200));
		
		panelTableAFND.setLayout(new BorderLayout());
		panelTableAFND.add(panelTableAFNDTop, BorderLayout.NORTH);
		panelTableAFND.add(scrollTableAFND);
		
		buttonTableAFNDSimplified.setText("Simplificar Tabela AFD");
		buttonTableAFNDSimplified.addActionListener(action -> createAFDSimplifiedTableAction());
		buttonTableAFNDSimplified.setFocusable(false);
		buttonTableAFNDSimplified.setPreferredSize(new Dimension(250, 25));
		
		panelTableAFNDSimplifiedButton.setLayout(new BorderLayout());
		panelTableAFNDSimplifiedButton.add(buttonTableAFNDSimplified, BorderLayout.EAST);
		
		labelTableAFNDSimplified.setText("Tabela AFD Simplificada:");
		
		panelTableAFNDSimplifiedTop.setLayout(new GridLayout(2, 1));
		panelTableAFNDSimplifiedTop.add(panelTableAFNDSimplifiedButton, BorderLayout.NORTH);
		panelTableAFNDSimplifiedTop.add(labelTableAFNDSimplified);
		
		tableAFDTransitionsSimplified.getTableHeader().setReorderingAllowed(false);
		scrollTableAFNDSimplified.setViewportView(tableAFDTransitionsSimplified);
		scrollTableAFNDSimplified.setPreferredSize(new Dimension(200, 200));
		
		panelTableAFNDSimplified.setLayout(new BorderLayout());
		panelTableAFNDSimplified.add(panelTableAFNDSimplifiedTop, BorderLayout.NORTH);
		panelTableAFNDSimplified.add(scrollTableAFNDSimplified);
		
		panelMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		panelMain.setResizeWeight(0.5);
		panelMain.setEnabled(false);
		panelMain.setDividerSize(15);
		panelMain.add(panelTableAFND);
		panelMain.add(panelTableAFNDSimplified);

		add(panelMain, BorderLayout.CENTER);
		
	}
	
	private void createAFDTableAction() {
		getManageBean().createAFDTableAction();
		tableAFDTransitions.setModel(getManageBean().getTableModelTransitionTableAFD());
	}

	private void createAFDSimplifiedTableAction() {
		getManageBean().createAFDSimplifiedTableAction();
		tableAFDTransitionsSimplified.setModel(getManageBean().getTableModelTransitionTableAFDSimplified());
	}
	
	public JTable getAFDTable() {
		return tableAFDTransitions;
	}
	
	public JTable getAFDSimplifiedTable() {
		return tableAFDTransitionsSimplified;
	}

	public void setButtonTableAFNDEnabled(Boolean enabled) {
		buttonTableAFND.setEnabled(enabled);
	}

	public void setButtonTableAFNDSimplifiedEnabled(Boolean enabled) {
		buttonTableAFNDSimplified.setEnabled(enabled);
	}
	
}
