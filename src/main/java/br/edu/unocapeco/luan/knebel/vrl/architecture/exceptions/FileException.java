package br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions;

import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumFileExceptionMessage;

public class FileException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public FileException(EnumFileExceptionMessage message) {
		super(message.getMessage());
	}
	
	public FileException(EnumFileExceptionMessage message, Throwable cause) {
		super(message.getMessage(), cause);
	}
	
	public FileException(Throwable cause) {
		super(cause);
	}
	

}
