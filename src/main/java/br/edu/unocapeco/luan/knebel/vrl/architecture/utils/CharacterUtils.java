package br.edu.unocapeco.luan.knebel.vrl.architecture.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CharacterUtils {

	public static char[] getValidTerminalSimbols() {
		char[] validTerminalSimbols = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
									   'p','q','r','s','t','u','v','w','x','y','z','0','1','2','3',
									   '4','5','6','7','8','8','-','+','*','/'};
		return validTerminalSimbols;
	}
	
	public static char[] getValidNonTerminalSimbols() {
		char[] validNonTerminalSimbols = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		return validNonTerminalSimbols;
	}
	
	public static char[] getValidSpecialSimbols() {
		char[] validSpecialSimbols = {'|','&', '='};
		return validSpecialSimbols;
	}
	
	public static String[] getValidCharacterGrammarMetaDataTerminals() {
		String[] validCaracters = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
				                   ",","0","1","2","3","4","5","6","7","8","9","-","+","*","/"};
		return validCaracters;
	}
	
	public static String[] getValidCharacterGrammarMetaDataNonTerminals() {
		String[] validCaracters = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",","};
		return validCaracters;
	}
	
	public static Integer getNumberByAphabetSimbol(String simbol) {
		Optional<Integer> number = getAlphabetMap().entrySet().stream().filter(map -> map.getValue().equals(simbol)).map(Map.Entry::getKey).findFirst();
		return number.orElse(null);
	}
	
	public static Map<Integer, String> getAlphabetMap() {
		Map<Integer, String> alphabetMap = new HashMap<>();
		
		alphabetMap.put(1, "A");
		alphabetMap.put(2, "B");
		alphabetMap.put(3, "C");
		alphabetMap.put(4, "D");
		alphabetMap.put(5, "E");
		alphabetMap.put(6, "F");
		alphabetMap.put(7, "G");
		alphabetMap.put(8, "H");
		alphabetMap.put(9, "I");
		alphabetMap.put(10, "J");
		alphabetMap.put(11, "K");
		alphabetMap.put(12, "L");
		alphabetMap.put(13, "M");
		alphabetMap.put(14, "N");
		alphabetMap.put(15, "O");
		alphabetMap.put(16, "P");
		alphabetMap.put(17, "Q");
		alphabetMap.put(18, "R");
		alphabetMap.put(19, "S");
		alphabetMap.put(20, "T");
		alphabetMap.put(21, "U");
		alphabetMap.put(22, "V");
		alphabetMap.put(23, "W");
		alphabetMap.put(24, "X");
		alphabetMap.put(25, "Y");
		alphabetMap.put(26, "Z");
		//restart alphabet with line
		alphabetMap.put(27, "A'");
		alphabetMap.put(28, "B'");
		alphabetMap.put(29, "C'");
		alphabetMap.put(30, "D'");
		alphabetMap.put(31, "E'");
		alphabetMap.put(32, "F'");
		alphabetMap.put(33, "G'");
		alphabetMap.put(34, "H'");
		alphabetMap.put(35, "I'");
		alphabetMap.put(36, "J'");
		alphabetMap.put(37, "K'");
		alphabetMap.put(38, "L'");
		alphabetMap.put(39, "M'");
		alphabetMap.put(40, "N'");
		alphabetMap.put(41, "O'");
		alphabetMap.put(42, "P'");
		alphabetMap.put(43, "Q'");
		alphabetMap.put(44, "R'");
		alphabetMap.put(45, "S'");
		alphabetMap.put(46, "T'");
		alphabetMap.put(47, "U'");
		alphabetMap.put(48, "V'");
		alphabetMap.put(49, "W'");
		alphabetMap.put(50, "X'");
		alphabetMap.put(51, "Y'");
		alphabetMap.put(52, "Z'");
		
		return alphabetMap;
	}
	
}
