package br.edu.unocapeco.luan.knebel.vrl.view.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.text.Document;

public class PanelTextFieldDescription extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private String labelDescription;
	private JLabel label;
	private JTextField textField;
	private int labelWidth;
	
	public PanelTextFieldDescription(String labelDescription, int labelWidth) {
		this.labelDescription = labelDescription;
		this.labelWidth = labelWidth;
		initComponets();
	}

	private void initComponets() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		
		textField = new JTextField();
		textField.setBorder(new LineBorder(Color.lightGray, 1, true));
		label = new JLabel(labelDescription + ": ");
		label.setPreferredSize(new Dimension(labelWidth, 10));
		
		add(label, BorderLayout.WEST);
		add(textField);
		
	}
	
	public void setDocument(Document document) {
		textField.setDocument(document);
	}
	
	public String getText() {
		return textField.getText().toString();
	}
	
	public void setText(String text) {
		textField.setText(text);
	}
	
	public JLabel getLabel() {
		return label;
	}

	public void setLabel(JLabel label) {
		this.label = label;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}
	
}
