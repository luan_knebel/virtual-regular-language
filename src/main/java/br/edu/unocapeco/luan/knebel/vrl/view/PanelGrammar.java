package br.edu.unocapeco.luan.knebel.vrl.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.text.html.HTMLEditorKit;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.architecture.documents.LowerCaseDocument;
import br.edu.unocapeco.luan.knebel.vrl.architecture.documents.UpperCaseDocument;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;
import br.edu.unocapeco.luan.knebel.vrl.view.components.PanelTextFieldDescription;

public class PanelGrammar extends AbstractPanel{

	private static final long serialVersionUID = 1L;
	
	private JLabel labelGrammarData = new JLabel();
	private JLabel labelGrammarModified = new JLabel();
	
	private JSplitPane panelMain;
	private JPanel panelGrammar = new JPanel();
	private JPanel panelGrammarData = new JPanel();
	private JPanel panelGrammarModified = new JPanel();
	private JPanel panelGrammarDescriptions = new JPanel();
	private JPanel panelGrammarButton = new JPanel();
	private JPanel panelGrammarCreateButtonModified = new JPanel();
	private JPanel panelGrammarButtonModified = new JPanel();
	private JPanel panelGrammarViewModified = new JPanel();
	
	private JEditorPane viewGramarModified = new JEditorPane();
	
	private PanelTextFieldDescription panelTexfDescriptionTerminals = new PanelTextFieldDescription("Terminais ∑", 130);
	private PanelTextFieldDescription panelTexfDescriptionNonTerminals = new PanelTextFieldDescription("Não Terminais N", 130);
	private JTextArea textAreaGrammar = new JTextArea();
	
	private JButton buttonValidateGrammar = new JButton();
	private JButton buttonCreateGrammarModified = new JButton();
	
	public PanelGrammar(VirtualRegularLanguageMBean mBean) {
		super(mBean);
		initComponets();
	}
	
	private void initComponets() {
		setLayout(new GridLayout(1, 2));
		setBorder(getPatternEmptyBorder());
		panelTexfDescriptionTerminals.setDocument(new LowerCaseDocument(50));
		panelTexfDescriptionNonTerminals.setDocument(new UpperCaseDocument(50));
		
		buttonValidateGrammar.setPreferredSize(new Dimension(200, 20));
		buttonValidateGrammar.setText("Validar Gramática Regular");
		buttonValidateGrammar.setFocusable(false);
		buttonValidateGrammar.addActionListener(action -> getManageBean().validateGrammarAction());
		
		panelGrammarButton.setLayout(new BorderLayout());
		panelGrammarButton.add(buttonValidateGrammar, BorderLayout.EAST);
		
		panelGrammarDescriptions.setLayout(new GridLayout(3, 1));
		panelGrammarDescriptions.add(panelGrammarButton);
		panelGrammarDescriptions.add(panelTexfDescriptionTerminals);
		panelGrammarDescriptions.add(panelTexfDescriptionNonTerminals);
		
		labelGrammarData.setText("Estrutura da Gramática:");
		textAreaGrammar.setBorder(new LineBorder(Color.lightGray, 1, true));
		
		textAreaGrammar.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				getManageBean().notifyGrammarAltered();
			}
		});
		
		panelGrammarData.setLayout(new BorderLayout());
		panelGrammarData.add(labelGrammarData, BorderLayout.NORTH);		
		panelGrammarData.add(textAreaGrammar);		
		
		panelGrammar.setLayout(new BorderLayout());
		panelGrammar.setPreferredSize(new Dimension(250, 25));
		panelGrammar.add(panelGrammarDescriptions, BorderLayout.NORTH);
		panelGrammar.add(panelGrammarData);
		
		HTMLEditorKit kit = new HTMLEditorKit();
		viewGramarModified.setEditorKit(kit);
		viewGramarModified.setEditable(false);
		viewGramarModified.setBackground(Color.WHITE);
		viewGramarModified.setBorder(new LineBorder(Color.lightGray, 1, true));
		
		buttonCreateGrammarModified.setPreferredSize(new Dimension(220, 25));
		buttonCreateGrammarModified.setText("Criar Gramática Modificada");
		buttonCreateGrammarModified.setFocusable(false);
		buttonCreateGrammarModified.addActionListener(action -> getManageBean().validateCreateGrammarModifiedAction());
		
		panelGrammarButtonModified.setLayout(new BorderLayout());
		panelGrammarButtonModified.add(buttonCreateGrammarModified, BorderLayout.EAST);
		
		panelGrammarCreateButtonModified.setLayout(new GridLayout(3, 1));
		panelGrammarCreateButtonModified.add(panelGrammarButtonModified);
		
		panelGrammarViewModified.setLayout(new BorderLayout());
		panelGrammarViewModified.add(labelGrammarModified, BorderLayout.NORTH);
		panelGrammarViewModified.add(viewGramarModified, BorderLayout.CENTER);
		
		labelGrammarModified.setText("Gramática Modificada:");
		panelGrammarModified.setLayout(new BorderLayout());
		panelGrammarModified.add(panelGrammarCreateButtonModified, BorderLayout.NORTH);
		panelGrammarModified.add(panelGrammarViewModified, BorderLayout.CENTER);
		panelGrammarModified.setPreferredSize(new Dimension(250, 25));
		
		panelMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		panelMain.setResizeWeight(0.5);
		panelMain.setEnabled(false);
		panelMain.setDividerSize(15);
		panelMain.add(panelGrammar);
		panelMain.add(panelGrammarModified);

		add(panelMain, BorderLayout.CENTER);
		
	}

	public JTextField getTextFieldTerminals() {
		return panelTexfDescriptionTerminals.getTextField();
	}

	public JTextField getTextFieldNotTerminals() {
		return panelTexfDescriptionNonTerminals.getTextField();
	}

	public JTextArea getTextAreaGrammar() {
		return textAreaGrammar;
	}
	
	public JEditorPane getEditorPanelGramar() {
		return viewGramarModified;
	}

	public void setButtonValidateGrammarEnabled(Boolean enabled) {
		buttonValidateGrammar.setEnabled(enabled);
	}

	public void setButtonCreateGrammarModifiedEnabled(Boolean enabled) {
		buttonCreateGrammarModified.setEnabled(enabled);
	}
	
}
