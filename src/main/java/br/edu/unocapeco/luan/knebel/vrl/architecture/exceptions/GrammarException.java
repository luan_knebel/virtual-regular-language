package br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions;

import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumGrammarExceptionMessage;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumProduceExceptionMessage;

public class GrammarException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	private static String grammarMessage = "\nSímbolo verificado: ";
	private static String produceMessage = "\nProdução Verificada: ";
	
	public GrammarException(EnumGrammarExceptionMessage message) {
		super(message.getMessage());
	}
	
	public GrammarException(EnumGrammarExceptionMessage message, String readSimbol) {
		super(message.getMessage() + grammarMessage + readSimbol);
	}
	
	public GrammarException(EnumGrammarExceptionMessage message, char readSimbol) {
		super(message.getMessage() + grammarMessage + readSimbol);
	}
	
	public GrammarException(EnumProduceExceptionMessage message) {
		super(message.getMessage());
	}
	
	public GrammarException(EnumProduceExceptionMessage message, String readSimbol) {
		super(message.getMessage() + produceMessage + readSimbol);
	}
	
	public GrammarException(EnumProduceExceptionMessage message, char readSimbol) {
		super(message.getMessage() + produceMessage + readSimbol);
	}
	
	public GrammarException(EnumGrammarExceptionMessage message, Throwable cause) {
		super(message.getMessage(), cause);
	}
	
	public GrammarException(Throwable cause) {
		super(cause);
	}
	

}
