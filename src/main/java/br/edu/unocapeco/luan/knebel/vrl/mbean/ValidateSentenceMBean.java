package br.edu.unocapeco.luan.knebel.vrl.mbean;

import java.util.List;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumAutomataExceptionMessage;
import br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions.AutomataException;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.beans.Transition;

public class ValidateSentenceMBean extends AbstractClientControll{

	private List<State> statesListAutomataValidation;
	
	public ValidateSentenceMBean(IClientDataControll clientControll) {
		super(clientControll);
	}
	
	public void validateSentence() {
		
		try {
			getClientDataControll().getTextAreaConsoleValidations().setText("");
			
			Thread thread = new Thread(new Runnable() {
		         public void run()
		         {
		        	 getClientButtonControll().setButtonValidateSentencesEnabled(false);
		        	 try {
		        		 runValidateSentence();
		        		 printMessageDialog("Sentença válida!");
					} catch (Exception e) {
						printMessageDialog(e.getMessage());
					}
		        	 getClientButtonControll().setButtonValidateSentencesEnabled(true);
		         }
			});
			thread.start();
		} catch (Exception e) {
			printMessageDialog(e.getMessage());
		}
		
	}

	private void runValidateSentence() throws AutomataException, InterruptedException {
		
		String sentence = getClientDataControll().getSentence();
		
		boolean verifyFinalState = false;
		boolean simbolFound = false;
		int recornizedSimbols = 0;
		State currentState;
		State initialState = null;
		
		for (State state : statesListAutomataValidation) {
			if (state.isFinal()) {
				verifyFinalState = true;
				break;
			}
		}

		if (!verifyFinalState) {
			throw new AutomataException(EnumAutomataExceptionMessage.FINAL_STATE_NOT_FOUND);
		}

		for (State state : statesListAutomataValidation) {
			if (state.isInitial()) {
				initialState = state;
				break;
			}
		}

		currentState = initialState;
		getClientDataControll().getPanelViewAutomata().setSelectedState(currentState);
		getClientDataControll().getPanelViewAutomata().repaint();
		Thread.sleep(1700);
		
		for (char currentSimbol : sentence.trim().toCharArray()) {
			
			simbolFound = false;
			for (Transition transition : currentState.getTransitions()) {
				
				for (Character transitionSimbols : transition.getSimbols()) {
					
					if (transitionSimbols.equals(Character.valueOf(currentSimbol))) {
						recornizedSimbols++;
						setCurrentInformationSentenceValidation(currentState, transition.getDestinyState(), currentSimbol);
						currentState = transition.getDestinyState();
						Thread.sleep(1700);
						simbolFound = true;
						break;
					}
				}
			}
			
			if(!simbolFound){
				setCurrentInformationInvalidSentenceValidation(currentState, currentSimbol);
				break;
			}
		}

		if(!(simbolFound && currentState.isFinal() && recornizedSimbols == sentence.length())){
			throw new AutomataException(EnumAutomataExceptionMessage.INVALID_SENTENCE);
		}
	}
	
	private void setCurrentInformationSentenceValidation(State currentState, State destinyState, char simbol) {
		StringBuilder textConsole = new StringBuilder();
		textConsole.append(getClientDataControll().getTextAreaConsoleValidations().getText());
		textConsole.append("Estado ");
		textConsole.append(currentState.getName());
		textConsole.append(" para o estado ");
		textConsole.append(destinyState.getName());
		textConsole.append(" com o símbolo ");
		textConsole.append(simbol);
		textConsole.append("\n");
		getClientDataControll().getTextAreaConsoleValidations().setText(textConsole.toString());
		getClientDataControll().getPanelViewAutomata().setSelectedState(destinyState);
		getClientDataControll().getPanelViewAutomata().repaint();
	}

	private void setCurrentInformationInvalidSentenceValidation(State currentState, char simbol) {
		StringBuilder textConsole = new StringBuilder();
		textConsole.append(getClientDataControll().getTextAreaConsoleValidations().getText());
		textConsole.append("\nERRO: Não foi encontrato um caminho \npara o símbolo ");
		textConsole.append("\"");
		textConsole.append(simbol);
		textConsole.append("\"");
		textConsole.append(" a partir do estado ");
		textConsole.append(currentState.getName());
		getClientDataControll().getTextAreaConsoleValidations().setText(textConsole.toString());
	}
	
	public List<State> getStatesListAutomataValidation() {
		return statesListAutomataValidation;
	}

	public void setStatesListAutomataValidation(List<State> statesListAutomataValidation) {
		this.statesListAutomataValidation = statesListAutomataValidation;
	}

}
