package br.edu.unocapeco.luan.knebel.vrl.beans;

import java.util.ArrayList;

import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.ValidationsUtils;

public class State {

    private String name;
    private boolean initial;
    private boolean finalState;
    private boolean finalControllState;
    private ArrayList< Transition > transitions;
    private int xCentral;
    private int yCentral;
    
    public State(){}
    
    public State(String name, boolean initial, boolean finalState) {
        this.name = name;
        this.initial = initial;
        this.finalState = finalState;
        this.finalControllState = false;
        transitions = new ArrayList< Transition >();
    }

    public boolean isFinal() {
        return finalState;
    }

    public boolean isInitial() {
        return initial;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Transition> getTransitions() {
        return transitions;
    }

    public int getXCentral() {
        return xCentral;
    }

    public int getYCentral() {
        return yCentral;
    }
    
    public void setFinal( boolean estFinal ) {
        this.finalState = estFinal;
    }
    
    public void setXCentral( int xCentral ) {
        this.xCentral = xCentral;
    }

    public void setYCentral( int yCentral ) {
        this.yCentral = yCentral;
    }
    
    public void setName(String name) {
		this.name = name;
	}

	public void setInitial(boolean initial) {
		this.initial = initial;
	}

	public void setTransitions(ArrayList<Transition> transitions) {
		this.transitions = transitions;
	}

	public State clone(){
		State other = new State();
		other.setFinal(finalState);
		other.setInitial(initial);
		other.setName(new String(name));
		other.setXCentral(xCentral);
		other.setYCentral(yCentral);
		other.setTransitions(new ArrayList<>(transitions));
		return other;
	}
	
	public boolean isFinalControllState() {
		return finalControllState;
	}

	public void setFinalControllState(boolean finalControllState) {
		this.finalControllState = finalControllState;
	}

	@Override
    public boolean equals( Object obj ) {
        if ( obj == null ) {
            return false;
        }
        if ( getClass() != obj.getClass() ) {
            return false;
        }
        final State other = ( State ) obj;
        if ( this.name != other.name && ( this.name == null || !this.name.equals( other.name ) ) ) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + ( this.name != null ? this.name.hashCode() : 0 );
        return hash;
    }
    
    @Override
    public String toString() {
        if ( initial && !finalState ) {
            return "\u2192" + name;
        } else if ( !initial && finalState ) {
            return "*" + name;
        } else if ( initial && finalState ) {
            return "\u2192*" + name;
        } else {
            return name;
        }
    }
    
    public boolean isTransationsEmpty() {
    	return ValidationsUtils.isCollectionNullOrEmpty(transitions);
	}
    
}
