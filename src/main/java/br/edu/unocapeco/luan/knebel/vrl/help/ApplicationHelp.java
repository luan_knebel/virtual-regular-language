package br.edu.unocapeco.luan.knebel.vrl.help;

import java.awt.Dimension;
import java.io.InputStream;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;
import org.icepdf.ri.util.PropertiesManager;

import br.edu.unocapeco.luan.knebel.vrl.architecture.resources.ApplicationResources;

public class ApplicationHelp {

	public static void openHelp() {
        InputStream inputStream = ApplicationResources.getApplicationHelpFile();
        SwingController swingController = new SwingController();
        SwingViewBuilder factoryViewBuilder = new SwingViewBuilder(swingController, getProperties());
        JPanel viewerComponentPanel = factoryViewBuilder.buildViewerPanel();
        Dimension defaultDimension = new Dimension(700, 800);
        JDialog dialogView = new JDialog(new JFrame(), "Ajuda", true);
        
        swingController.openDocument(inputStream, "Ajuda", null);
        dialogView.setPreferredSize(defaultDimension);
        dialogView.getContentPane().add(viewerComponentPanel);
        dialogView.pack();
        dialogView.setVisible(true);
	}
	
	private static PropertiesManager getProperties() {
		Properties properties = new Properties();
		properties.setProperty("application.showLocalStorageDialogs", Boolean.FALSE.toString());
        PropertiesManager propertiesManager = new PropertiesManager(System.getProperties(), properties, ResourceBundle.getBundle(PropertiesManager.DEFAULT_MESSAGE_BUNDLE));
        propertiesManager.setBoolean(PropertiesManager.PROPERTY_SHOW_UTILITY_SAVE, Boolean.FALSE);
        propertiesManager.setBoolean(PropertiesManager.PROPERTY_SHOW_UTILITY_OPEN, Boolean.FALSE);
        propertiesManager.setBoolean(PropertiesManager.PROPERTY_SHOW_TOOLBAR_ANNOTATION, Boolean.FALSE);
        propertiesManager.setBoolean(PropertiesManager.PROPERTY_SHOW_UTILITYPANE_ANNOTATION, Boolean.FALSE);
        return  propertiesManager;
	}
}
