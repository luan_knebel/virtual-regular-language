package br.edu.unocapeco.luan.knebel.vrl.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;
import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelViewAutomata;

public class PanelTransitionTable extends AbstractPanel{

	private static final long serialVersionUID = 1L;
	
	private JLabel labelTableTransition = new JLabel();
	private JLabel labelAutomataGrammar = new JLabel();
	
	private JSplitPane panelMain;
	private JPanel panelMainTableTransitions = new JPanel();
	private JPanel panelButtonGenerateTable = new JPanel();
	private JPanel panelTableTransition = new JPanel();
	private JPanel panelAutomataGrammar = new JPanel();
	private JPanel panelGenerateAutomata = new JPanel();
	private JPanel panelButtonGenerateAutomata = new JPanel();
	
	private JButton buttonGenerateTable = new JButton();
	private JButton buttonGenerateAutomata = new JButton();
	
	private JTable tableTransitions = new JTable();
	private JScrollPane scrollTable = new JScrollPane();
	
	private PanelViewAutomata panelViewAutomataGrammar;
	
	public PanelTransitionTable(VirtualRegularLanguageMBean mBean) {
		super(mBean);
		initComponets();
	}

	private void initComponets() {
		setLayout(new BorderLayout());
		setBorder(getPatternEmptyBorder());
		panelViewAutomataGrammar = new PanelViewAutomata(getManageBean());
		
		buttonGenerateTable.setText("Gerar Tabela de Transição de Estados");
		buttonGenerateTable.setPreferredSize(new Dimension(290, 25));
		buttonGenerateTable.setFocusable(false);
		buttonGenerateTable.addActionListener(action -> createTableGrammarTransitionAction());
		
		panelButtonGenerateTable.setLayout(new BorderLayout());
		panelButtonGenerateTable.setBorder(BorderFactory.createEmptyBorder(0, 0, 7, 0));
		panelButtonGenerateTable.add(buttonGenerateTable, BorderLayout.EAST);
		
		labelTableTransition.setText("Tabela de Transição da Gramática:");
		tableTransitions.getTableHeader().setReorderingAllowed(false);
		scrollTable.setPreferredSize(new Dimension(100,100));
		scrollTable.setViewportView(tableTransitions);
		
		panelTableTransition.setLayout(new BorderLayout());
		panelTableTransition.add(labelTableTransition, BorderLayout.NORTH);
		panelTableTransition.add(scrollTable, BorderLayout.CENTER);
		
		panelMainTableTransitions.setLayout(new BorderLayout());
		panelMainTableTransitions.add(panelButtonGenerateTable, BorderLayout.NORTH);
		panelMainTableTransitions.add(panelTableTransition, BorderLayout.CENTER);
		
		buttonGenerateAutomata.setText("Gerar Automato AFND da Gramática");
		buttonGenerateAutomata.setPreferredSize(new Dimension(290, 25));
		buttonGenerateAutomata.setFocusable(false);
		buttonGenerateAutomata.addActionListener(action -> getManageBean().createAutomataAFNDAction());
		panelButtonGenerateAutomata.setLayout(new BorderLayout());
		panelButtonGenerateAutomata.add(buttonGenerateAutomata, BorderLayout.EAST);
		labelAutomataGrammar.setText("Autômato AFND da Gramática:");
		
		panelGenerateAutomata.setLayout(new GridLayout(2, 1));
		panelGenerateAutomata.add(panelButtonGenerateAutomata, BorderLayout.EAST);
		panelGenerateAutomata.add(labelAutomataGrammar);
		
		panelAutomataGrammar.setLayout(new BorderLayout());
		panelAutomataGrammar.add(panelGenerateAutomata, BorderLayout.NORTH);
		panelAutomataGrammar.add(panelViewAutomataGrammar);
		
		panelMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		panelMain.setResizeWeight(0.7);
		panelMain.setEnabled(false);
		panelMain.setDividerSize(15);
		panelMain.add(panelAutomataGrammar);
		panelMain.add(panelMainTableTransitions);

		add(panelMain, BorderLayout.CENTER);
		
	}
	
	private void createTableGrammarTransitionAction() {
		getManageBean().createTableGrammarTransitionAction();
		tableTransitions.setModel(getManageBean().getTableModelTransitionTableGrammar());
	}
	
	public PanelViewAutomata getPanelViewAutomataGrammar() {
		return panelViewAutomataGrammar;
	}
	
	public JTable getGrammarTable() {
		return tableTransitions;
	}

	public void setButtonGenerateTableEnabled(Boolean enabled) {
		buttonGenerateTable.setEnabled(enabled);
	}

	public void setButtonGenerateAutomataTableEnabled(Boolean enabled) {
		buttonGenerateAutomata.setEnabled(enabled);
	}
}
