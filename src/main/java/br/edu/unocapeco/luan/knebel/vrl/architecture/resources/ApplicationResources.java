package br.edu.unocapeco.luan.knebel.vrl.architecture.resources;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.OptionPaneUtils;

public class ApplicationResources {
	
	public static  Image getApplicationLogo(){
		return Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icons/logo.png"));
	}
	
	public static InputStream getApplicationHelpFile(){
		return ClassLoader.getSystemResourceAsStream("help/help.pdf");
	}
	
	public static String getApplicationName(){
		Properties properties = getApplicationProperties();
		return properties.getProperty("name") + " - " + properties.getProperty("version");
	}
	
	public static Properties getApplicationProperties(){
		Properties properties = new Properties();
		try {
			
			InputStream stream = ApplicationResources.class.getClassLoader().getResourceAsStream("pom.properties");
			if(stream != null){
				properties.load(stream);
				stream.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return properties;
	}
	
	public static String getJavaVersion() {
		 return System.getProperty("java.version");
	}
	
	
}
