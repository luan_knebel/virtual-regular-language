package br.edu.unocapeco.luan.knebel.vrl.architecture.client;

public interface IClientButtonControll{

	void setButtonValidateGrammarEnabled(Boolean enabled);
	
	void setButtonCreateGrammarModifiedEnabled(Boolean enabled);
	
	void setButtonGenerateTableEnabled(Boolean enabled);
	
	void setButtonGenerateAutomataTableEnabled(Boolean enabled);
	
	void setButtonTableAFNDEnabled(Boolean enabled);
	
	void setButtonTableAFNDSimplifiedEnabled(Boolean enabled);
	
	void setButtonGenerateAutomataValidateEnabled(Boolean enabled);
	
	void setButtonValidateSentencesEnabled(Boolean enabled);
}
