package br.edu.unocapeco.luan.knebel.vrl.architecture.enums;

public enum EnumFileExceptionMessage {

	INVALID_FILE("Arquivo inválido!\nSelecione um arquivo com extenção .grammar!"),
	READ_ERROR("Erro ao ler arquivo!"),
	WRITE_ERROR("Erro ao gravar arquivo!"),
	DATA_EMPTY("Informe todos os dados da Gramática para salvar o arquivo!");
	
	private String message;
	
	EnumFileExceptionMessage(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
}
