package br.edu.unocapeco.luan.knebel.vrl.main;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.jtattoo.plaf.mcwin.McWinLookAndFeel;

import br.edu.unocapeco.luan.knebel.vrl.controls.ApplicationJavaVersionControll;
import br.edu.unocapeco.luan.knebel.vrl.view.FrameVirtualRegularLanguage;

public class RunnableApplication {

	public static void main(String[] args) {
		configureLookAndFell();
		validateJavaVersion();
		new FrameVirtualRegularLanguage();
	}

	private static void validateJavaVersion() {
		ApplicationJavaVersionControll.validateJavaVersion();
	}
	
	private static void configureLookAndFell() {
		try {
			McWinLookAndFeel.setTheme("Large-Font", "", "VRL");
			UIManager.setLookAndFeel(new McWinLookAndFeel());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

}
