package br.edu.unocapeco.luan.knebel.vrl.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.CharacterUtils;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.beans.Transition;

public class TransformationAFNDToAFDMBean extends AbstractClientControll{

	private List<State> originalStates;	
	private List<State> AFDListStates;
	private List<State> newListStatesChecked;
	private List<State> AFDListStatesSimplified;
	private Character[] allTerminalsArray;
	
	public TransformationAFNDToAFDMBean(IClientDataControll clientControll) {
		super(clientControll);
	}

	public void transformAFNDToAFD() throws Exception{
		
		AFDListStates = new ArrayList<>();
		newListStatesChecked = new ArrayList<>();
		
		allTerminalsArray = getDistinctTerminalsOfAllStates();
		
		for (State currentState :originalStates) {
			
			if(currentState.isInitial()){
				State newState = new State(currentState.getName(), true, currentState.isFinal());
				addStateOfAFDLisStates(newState);
			}
			
			if(existsDuplicateTerminalsOfState(currentState)){
				
				for (int i = 0; i < allTerminalsArray.length; i++) {
					
					Character currentTerminal = allTerminalsArray[i];
					
					State newState = getNewDestinyStateOfStateAndSimbol(currentState, currentTerminal);
					if(newState == null) continue;
					addStateOfAFDLisStates(newState);
					addTransition(currentState.getName(), newState.getName(), String.valueOf(currentTerminal));
					
				}
				
				verifyAllNewStatesOfCurrentState();
			}else{
				
				for (int i = 0; i < allTerminalsArray.length; i++) {
					
					Character currentTerminal = allTerminalsArray[i];
					State newCurrentState = new State(currentState.getName(), false, currentState.isFinal());
					addStateOfAFDLisStates(newCurrentState);
					
					State destinyState = getDestinyStateOfStateAndSimbol(currentState.getName(), currentTerminal);
					if(destinyState == null) continue;
					
					addStateOfAFDLisStates(destinyState);
					addTransition(currentState.getName(), destinyState.getName(), String.valueOf(currentTerminal));
				}
			}
		}
	}
	
	private State getDestinyStateOfStateAndSimbol(String stateName, Character simbol) {
		
		for (State curretState : originalStates) {
			if(curretState.getName().equals(stateName)){
				for (Transition transition : curretState.getTransitions()) {
					if(transition.getSimbols().contains(simbol)){
						State destinyState = transition.getDestinyState();
						return new State(destinyState.getName(), false, destinyState.isFinal());
					}
				}
			}
		}
		return null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void addTransition(String stateName, String destinyStateName, String simbol) {
		
		State originState = getStateByName(AFDListStates, stateName);
		State destinyState = getStateByName(AFDListStates, destinyStateName);
		
    	if (originState == null && simbol.trim().equals("")) return;
    	
        boolean isTransactionExist = false;
        Transition transactionExisting = null;

        for (Transition t : originState.getTransitions()) {
            if (t.getDestinyState().equals(destinyState)) {
                isTransactionExist = true;
                transactionExisting = t;
                break;
            }
        }
        
        if (!isTransactionExist) {
            TreeSet setSimbols = new TreeSet< Character >();
            setSimbols.add(simbol.trim().charAt(0));
            originState.getTransitions().add(new Transition(setSimbols,destinyState));
        } else {
            transactionExisting.getSimbols().add(simbol.trim().charAt(0));
        }
		
        updateStateList(AFDListStates, originState);
        updateStateList(AFDListStates, destinyState);
		
	}
	
	private State getStateByName(List<State> statesList, String stateName) {
		for (State state : statesList) {
			if(state.getName().equals(stateName))
				return state;
		}
		return null;
	}
	
	private void updateStateList(List<State> statesList, State stateUpdatable) {
		statesList.forEach(state ->{
			if(state.getName().equals(stateUpdatable.getName())){
				state = stateUpdatable;
			}
		});
	}
	
	private void addStateOfAFDLisStates(State state) {
		
		long countState = AFDListStates.stream().filter(s -> s.getName().equals(state.getName())).count();
		
		if(countState == 0){
			AFDListStates.add(state);
		}
	}
	
	private State getNewDestinyStateByStateNameAndSimbol(String stateName, Character simbol) {
		
		List<String> listStateByName = getListNameByStateName(stateName);
		List<String> listStateName = new ArrayList<>();
		String newStateName = "";
		
		for (String name : listStateByName) {
			
			List<String> tempStateName = getAllStateNameByOriginStateName(name, simbol);
			if(tempStateName.isEmpty()) continue;
			listStateName.addAll(tempStateName);
			
		}
		if(listStateName.isEmpty()) return null;
		
		TreeSet<String> treeSetListStateName = new TreeSet<String>(listStateName);
		
		for (String name : treeSetListStateName) {
			newStateName += name;
		}
		
		boolean terminalState = getIsStateFinalByName(newStateName);
		return new State(newStateName, false, terminalState);
	}
	
	private List<String> getAllStateNameByOriginStateName(String stateName, Character simbol) {
		List<String> newStateName = new ArrayList<>();
		
		for (State originalState : originalStates) {
			if(originalState.getName().equals(stateName)){
				for (Transition transition : originalState.getTransitions()) {
					if(transition.getSimbols().contains(simbol)){
						newStateName.add(transition.getDestinyState().getName());
					}
				}
			}
		}
		
		return newStateName;
	}
	
	private State getNewDestinyStateOfStateAndSimbol(State currentState, Character simbol) {
		List<String> listStateName = new ArrayList<>();
		String stateName = "";
		
		for (Transition transitions : currentState.getTransitions()) {
			if(transitions.getSimbols().contains(simbol)){
				listStateName.add(transitions.getDestinyState().getName());
			}
		}
		
		if(listStateName.isEmpty()) return null;
		
		sortAlphabetOrder(listStateName);
		for (String name : listStateName) {
			stateName += name;
		}
		boolean terminalState = getIsStateFinalByName(stateName);
		return new State(stateName, false, terminalState);
	}
	
	
	private boolean getIsStateFinalByName(String stateName) {
		
		List<String> listStateName = getListNameByStateName(stateName);
		
		for (String name : listStateName) {
			for (State state : originalStates) {
				if(state.getName().equals(name)){
					if(state.isFinal()){
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	
	private List<String> getListNameByStateName(String stateName) {
		
		char[] nameArray = stateName.toCharArray();
		List<String> listaNameStates = new ArrayList<>();
		String nameState;
		
		for (int i = 0; i < nameArray.length; i++) {
			
			nameState = String.valueOf(nameArray[i]);
			
			if((i+1) < nameArray.length){
				
				if(nameArray[i+1] == '\''){
					nameState += String.valueOf(nameArray[i+1]);
					i++;
				}
			}
			listaNameStates.add(nameState);
		}
		
		return listaNameStates;
	}
	
	private void verifyAllNewStatesOfCurrentState() {
		
		List<State> tempListState = getCopyOfAFDListStates();
		
		for (State state : tempListState) {
			
			if(isStateChecked(state)) continue;
			
			for (int i = 0; i < allTerminalsArray.length; i++) {
				
				Character currentTerminal = allTerminalsArray[i];
				State newState = getNewDestinyStateByStateNameAndSimbol(state.getName(), currentTerminal);
				if(newState == null) continue;
				addStateOfAFDLisStates(newState);
				addTransition(state.getName(), newState.getName(), String.valueOf(currentTerminal));
				
			}
			newListStatesChecked.add(state);
		}
		
		if(AFDListStates.size() > tempListState.size()){
			verifyAllNewStatesOfCurrentState();
		}
	}
	
	private List<State> getCopyOfAFDListStates() {
		List<State> listState = new ArrayList<>();
		for (State state : AFDListStates) {
			listState.add(state);
		}
		return listState;
	}

	
	private List<State> getCopyOfAFDListStatesToTransformAFD(List<State> AFDListStatesToTransformAFD) {
		List<State> listState = new ArrayList<>();
		for (State state : AFDListStatesToTransformAFD) {
			listState.add(state.clone());
		}
		
		listState.forEach(state ->{
			state.getTransitions().forEach(transition ->{
				State destinyState = getStateByName(listState, transition.getDestinyState().getName());
				transition.setDestinyState(destinyState);
			});
		});
		
		return listState;
	}
	
	private boolean isStateChecked(State state) {
		
		long countOriginalState = originalStates.stream().filter(s -> s.getName().equals(state.getName())).count();
		if(countOriginalState == 1) return true;
		
		long countStateChecked = newListStatesChecked.stream().filter(s -> s.getName().equals(state.getName())).count();
		return (countStateChecked == 1);
	}
	
	private Character[] getDistinctTerminalsOfAllStates() {

		List<Character> listTerminals = new ArrayList<>();
		Character[] arrayTerminals;
		
		for (State state :originalStates) {
			for (Transition transition : state.getTransitions()) {
				for (Character simbol : transition.getSimbols()) {
					if(Collections.frequency(listTerminals, simbol) == 0){
						listTerminals.add(simbol);
					}
				}
			}
		}
		
		arrayTerminals = new Character[listTerminals.size()];
		arrayTerminals = listTerminals.toArray(arrayTerminals);
		return arrayTerminals;
	}
	
	private boolean existsDuplicateTerminalsOfState(State state) {
		
		Character[] arrayTerminals = getAllTerminalsOfState(state);
		
		List<Character> listTerminals = Arrays.asList(arrayTerminals);
		
		for (Character simbol : listTerminals) {
			
			if(Collections.frequency(listTerminals, simbol) > 1){
				return true;
			}
		}
		
		return false;
	}
	
	private Character[] getAllTerminalsOfState(State state) {

		List<Character> listTerminals = new ArrayList<>();
		Character[] arrayTerminals;
		
		for (Transition transition : state.getTransitions()) {
			for (Character simbol : transition.getSimbols()) {
				listTerminals.add(simbol);
			}
		}
		
		arrayTerminals = new Character[listTerminals.size()];
		arrayTerminals = listTerminals.toArray(arrayTerminals);
		return arrayTerminals;
	}
	
	private void sortAlphabetOrder(List<String> list) {
		  Collections.sort(list, new Comparator<String>() {
		      @Override
		      public int compare(final String object1, final String object2) {
		          return object1.compareTo(object2);
		      }
		  });
	}
	
	public List<State> transformAFDToAFDSimplified(List<State> AFDListStatesToTransformAFD) {
		
		String stateName;
		String newStateName;

		AFDListStatesSimplified = getCopyOfAFDListStatesToTransformAFD(AFDListStatesToTransformAFD);
		
		for (State state : AFDListStatesSimplified) {
			
			stateName = state.getName();
			
			if(isStateUpdatable(stateName)){
				newStateName = getNewStateName();
				updateStateName(stateName, newStateName);
			}
			
		}
		
		return AFDListStatesSimplified;
	}
	
	private String getNewStateName() {
		
		Map<Integer, String> alphabetMap = CharacterUtils.getAlphabetMap();
		
		boolean findNewStateName = false;
		String newStateName = "";
		int letterNumber = 1;
		
		while (!findNewStateName) {
		
			newStateName = alphabetMap.get(letterNumber);
			findNewStateName = !existStateWithName(newStateName);
			letterNumber++;
		}
		
		return newStateName;
	}
	
	private boolean existStateWithName(String stateName) {
		for (State state : AFDListStatesSimplified) {
			if(state.getName().equals(stateName)) return true;
		}
		return false;
	}

	private void updateStateName(String stateName, String newStateName) {
		
		AFDListStatesSimplified.forEach(state ->{
			
			if(state.getName().equals(stateName)){
				state.setName(newStateName);
			}
			
			state.getTransitions().forEach(transition ->{
				if(transition.getDestinyState().getName().equals(stateName)){
					transition.getDestinyState().setName(newStateName);
				}
			});
		});
		
	}
	
	private boolean isStateUpdatable(String stateName) {
		if(stateName.length() == 1) return false;
		if(stateName.length() == 2 && stateName.charAt(1) == '\'') return false;
		return true;
	}
	
	public List<State> getOriginalStates() {
		return originalStates;
	}

	public void setOriginalStates(List<State> originalStates) {
		this.originalStates = originalStates;
	}

	public List<State> getAFDListStates() {
		return AFDListStates;
	}

	public void setAFDListStates(List<State> aFDListStates) {
		AFDListStates = aFDListStates;
	}

	public List<State> getAFDListStatesSimplified() {
		return AFDListStatesSimplified;
	}

	public void setAFDListStatesSimplified(List<State> aFDListStatesSimplified) {
		AFDListStatesSimplified = aFDListStatesSimplified;
	}

}
