package br.edu.unocapeco.luan.knebel.vrl.mbean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.apache.commons.lang3.ArrayUtils;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumGrammarExceptionMessage;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumProduceExceptionMessage;
import br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions.GrammarException;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.CharacterUtils;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.ValidationsUtils;
import br.edu.unocapeco.luan.knebel.vrl.beans.Grammar;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.beans.Transition;

public class GrammarMBean extends AbstractClientControll{
	
	private Grammar grammar;
	
	private char[] validTerminalSimbols = CharacterUtils.getValidTerminalSimbols();
	private char[] validNonTerminalSimbols = CharacterUtils.getValidNonTerminalSimbols();
	private char[] validSpecialSimbols = CharacterUtils.getValidSpecialSimbols();
	
	public GrammarMBean(IClientDataControll clientControll) {
		super(clientControll);
	}
	
	public void validateGrammar() throws GrammarException{
		if(ValidationsUtils.isObjectNull(grammar) 
				|| ValidationsUtils.isStringNullOrEmpty(grammar.getTerminalSimbols()) 
				|| ValidationsUtils.isStringNullOrEmpty(grammar.getNonTerminalSimbols()) 
				|| ValidationsUtils.isStringNullOrEmpty(grammar.getGrammarStructure()))
			throw new GrammarException(EnumGrammarExceptionMessage.GRAMMAR_DATA_EMPTY);
		
		validateGrammarCaracters();
		validateGrammarStructure();
	}

	private void validateGrammarCaracters() throws GrammarException{
		
		char simbol = ' ';
		String grammarStructure;
		String[] grammarTerminals;
		String[] grammarNonTerminals;
		boolean valid = false;
		
		grammarStructure = grammar.getGrammarStructure();
		grammarStructure = grammarStructure.replaceAll("\\s", "");
		grammarTerminals = grammar.getTerminalSimbols().split(",");
		grammarNonTerminals = grammar.getNonTerminalSimbols().split(",");
		
		for (int i = 0; i < grammarStructure.length(); i++) {
			
			simbol = grammarStructure.charAt(i);
			valid = ArrayUtils.contains(validTerminalSimbols, simbol);
			if(!valid) valid = ArrayUtils.contains(validNonTerminalSimbols, simbol);
			if(!valid) valid = ArrayUtils.contains(validSpecialSimbols, simbol);
			if(!valid) break;
		}
		
		if(!valid) throw new GrammarException(EnumGrammarExceptionMessage.INVALID_CARACTER_GRAMMAR,simbol);
		
		for (int i = 0; i < grammarTerminals.length; i++) {
			
			simbol = grammarTerminals[i].charAt(0);
			valid = (ArrayUtils.contains(validTerminalSimbols, simbol) && (grammarTerminals[i].length() == 1));
			if(!valid) break;
		}
		
		if(!valid) throw new GrammarException(EnumGrammarExceptionMessage.INVALID_CARACTER_TERMINAL_FIELD_GRAMMAR,simbol);
		
		for (int i = 0; i < grammarNonTerminals.length; i++) {
			
			simbol = grammarNonTerminals[i].charAt(0);
			valid = (ArrayUtils.contains(validNonTerminalSimbols, simbol) && (grammarNonTerminals[i].length() == 1));
			if(!valid) break;
		}
		
		if(!valid) throw new GrammarException(EnumGrammarExceptionMessage.INVALID_CARACTER_NON_TERMINAL_FIELD_GRAMMAR,simbol);
	}
	
	private void validateGrammarStructure() throws GrammarException{
		
		StringTokenizer linesGrammar;
		String[] grammarTerminals;
		String[] grammarNonTerminals;
		String nonTerminalContext;
		String[] nonTerminalContextArray;
		List<String> nonTerminalContextList = new ArrayList<String>();
		List<String>  producesList = new ArrayList<String>();
		int lineCont = 1;
		String line;
		String lineProduces;
		String contextProduces;
		String initialNonTerminalName = "";
		
		grammarTerminals = grammar.getTerminalSimbols().split(",");
		grammarNonTerminals = grammar.getNonTerminalSimbols().split(",");
		linesGrammar = new StringTokenizer(grammar.getGrammarStructure(),"\n");
		
		while (linesGrammar.hasMoreElements()) {
			
			  line = linesGrammar.nextToken();
			  lineProduces = line;
			  line = line.replaceAll("\\s", "");
			  
			  if(line.isEmpty()) continue;
			  
			  if(line.length() <= 1)
				  throw new GrammarException(EnumGrammarExceptionMessage.INVALID_GRAMMAR);
			  
			for (int i = 0; i < line.length(); i++) {
				  
				if(i == 0){
					nonTerminalContext = String.valueOf(line.charAt(0));
					
					if(lineCont == 1){
						initialNonTerminalName = nonTerminalContext;
					}
					
					validateDuplicatedNonTerminalGrammar(nonTerminalContextList, nonTerminalContext);
					nonTerminalContextList.add(nonTerminalContext);
					if(!ArrayUtils.contains(grammarNonTerminals, nonTerminalContext))
						throw new GrammarException(EnumGrammarExceptionMessage.NON_TERMINAL_NOT_CONTAINED_FIELD_GRAMMAR,nonTerminalContext);
				}
				  
				if(i == 1){
					if(line.charAt(1) != '='){
						throw new GrammarException(EnumGrammarExceptionMessage.SIMBOL_EQUALS_NOT_FOUND, line.charAt(1));
					}
				}
				
			    if(line.length() <= 2)
				    throw new GrammarException(EnumProduceExceptionMessage.PRODUCES_EMPTY);
				
				if(i == 2){
					contextProduces = lineProduces.substring(lineProduces.indexOf("=") + 1, lineProduces.length()).trim();
					validateCaractersProduce(contextProduces, grammarTerminals, grammarNonTerminals);
					producesList.add(contextProduces);
					break;
				}
			}
			lineCont++;
		}
		
		nonTerminalContextArray = new String[nonTerminalContextList.size()];
		nonTerminalContextArray = nonTerminalContextList.toArray(nonTerminalContextArray);
		validateProduces(nonTerminalContextArray, grammarTerminals, producesList, initialNonTerminalName);

	}
	
	private void validateDuplicatedNonTerminalGrammar(List<String> nonTerminalContextList, String nonTerminalContext) throws GrammarException {
		
		for (String nonTerminal : nonTerminalContextList) {
			if(nonTerminal.equals(nonTerminalContext))
				throw new GrammarException(EnumGrammarExceptionMessage.NON_TERMINAL_GRAMMAR_DUPLICATED,nonTerminalContext);
		}
	}
	
	private void validateCaractersProduce(String contextProduce, String[] grammarTerminals, String[] grammarNonTerminals) throws GrammarException{
		
		if(contextProduce.startsWith("|"))
			throw new GrammarException(EnumGrammarExceptionMessage.INVALID_CARACTERS_PRODUCES, "|");
		
		if(contextProduce.endsWith("|"))
			throw new GrammarException(EnumGrammarExceptionMessage.INVALID_CARACTERS_PRODUCES, "|");
		
		char[] produceCaraters = contextProduce.toCharArray();
		String simbol = "";
		boolean valid = false;
		
		for (int i = 0; i < produceCaraters.length; i++) {
			
			simbol = String.valueOf(produceCaraters[i]);
			if(simbol.equals(" ")) continue;
			
			valid = ArrayUtils.contains(grammarTerminals, simbol);
			if(!valid) valid = ArrayUtils.contains(grammarNonTerminals, simbol);
			if(!valid) valid = (simbol.equals("|") || simbol.equals("&"));
			if(!valid) break;
		}
		if(!valid)
			throw new GrammarException(EnumGrammarExceptionMessage.INVALID_CARACTERS_PRODUCES,simbol);
		
	}
	
	private void validateProduces(String[] nonTerminalContextArray, String[] grammarTerminals, List<String> producesList, String initialNonTerminalName) throws GrammarException {
		
		String[] produces;
		String produce;
		List<String> nonTerminalProducesList = new ArrayList<String>();
		String[] nonTerminalProducesArray;
		
		for (String grammarProduce : producesList) {
			
			produces = grammarProduce.split("\\|");
			
			for (int i = 0; i < produces.length; i++) {
				
				produce = produces[i].trim();
				
				if(produce.length() > 2 || produce.length() == 0)
					throw new GrammarException(EnumProduceExceptionMessage.INVALID_PRODUCE,produce);
				
				if(!ArrayUtils.contains(grammarTerminals, String.valueOf(produce.charAt(0))) && produce.charAt(0) != '&')
					throw new GrammarException(EnumProduceExceptionMessage.INVALID_PRODUCE_TERMINAL,produce);
				
				if(produce.length() == 2){
					if(!ArrayUtils.contains(nonTerminalContextArray, String.valueOf(produce.charAt(1))))
							throw new GrammarException(EnumProduceExceptionMessage.INVALID_PRODUCE_NON_TERMINAL_FOUND_GRAMMAR,produce);
					nonTerminalProducesList.add(String.valueOf(produce.charAt(1)));
				}
			}
		}
		
		nonTerminalProducesArray = new String[nonTerminalProducesList.size()];
		nonTerminalProducesArray = nonTerminalProducesList.toArray(nonTerminalProducesArray);
		
		for (int i = 0; i < nonTerminalContextArray.length; i++) {
			if((!ArrayUtils.contains(nonTerminalProducesArray, nonTerminalContextArray[i])) && nonTerminalContextArray[i] != initialNonTerminalName)
				throw new GrammarException(EnumGrammarExceptionMessage.NON_TERMIAL_NOT_USED,nonTerminalContextArray[i]);
		}
	}
	
	public List<State> getAutomataStructure() throws GrammarException{
		
		List<State> statesList = new ArrayList<State>();
		statesList = createStatesList();
		createTransitionsState(statesList);
		verifyGrammarType(statesList);
		
		return statesList;
	}
	
	private void createTransitionsState(List<State> statesList) {
		
		StringTokenizer linesGrammar;
		String line;
		String nameCurrentState;
		String nameStateDestiny;
		State currentState;
		State stateDestiny;
		String produce;
		String simbol;
		String contextProduces;
		String contextProducesArray[];
		
		linesGrammar = new StringTokenizer(grammar.getGrammarStructure(),"\n");
		
		while (linesGrammar.hasMoreElements()) {
			
			line = linesGrammar.nextToken();
			line = line.replaceAll("\\s", "");
			if(line.isEmpty()) continue;
			  
			nameCurrentState = String.valueOf(line.charAt(0));
			currentState = getStateByName(statesList, nameCurrentState);
			
			contextProduces = line.substring(line.indexOf("=") + 1, line.length()).trim();
			contextProducesArray = contextProduces.split("\\|");
			
			for (int i = 0; i < contextProducesArray.length; i++) {
				
				produce = contextProducesArray[i];
				
				if(produce.length() == 2){
					simbol = String.valueOf(produce.charAt(0));
					nameStateDestiny = String.valueOf(produce.charAt(1));
					stateDestiny = getStateByName(statesList, nameStateDestiny);
					addTransition(simbol, currentState, stateDestiny);
					updateStateList(statesList, currentState);
				}
				
				if(produce.length() == 1 && !produce.equals("&")){
					simbol = produce;
					nameStateDestiny = getFinalControllStateName(statesList);
					stateDestiny = getStateByName(statesList, nameStateDestiny);
					addTransition(simbol, currentState, stateDestiny);
					updateStateList(statesList, currentState);
				}
			}
		}
	}
	
	private String getFinalControllStateName(List<State> statesList) {
		for (State state : statesList) {
			if(state.isFinalControllState()) return state.getName();
		}
		return "";
	}
	
	private State getStateByName(List<State> statesList, String stateName) {
		for (State state : statesList) {
			if(state.getName().equals(stateName))
				return state;
		}
		return null;
	}
	
	private void updateStateList(List<State> statesList, State stateUpdatable) {
		statesList.forEach(state ->{
			if(state.getName().equals(stateUpdatable.getName())){
				state = stateUpdatable;
			}
		});
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
    private void addTransition(String simbol, State originState, State destinyState) {                                                
        
    	if (originState == null && simbol.trim().equals("")) return;
    	
        boolean isTransactionExist = false;
        Transition transactionExisting = null;

        for (Transition t : originState.getTransitions()) {
            if (t.getDestinyState().equals(destinyState)) {
                isTransactionExist = true;
                transactionExisting = t;
                break;
            }
        }
        
        if (!isTransactionExist) {
            TreeSet setSimbols = new TreeSet< Character >();
            setSimbols.add(simbol.trim().charAt(0));
            originState.getTransitions().add(new Transition(setSimbols,destinyState));
        } else {
            transactionExisting.getSimbols().add(simbol.trim().charAt(0));
        }
    }
	
	
	private List<State> createStatesList() {
		
		List<State> statesList = new ArrayList<State>();
		State state;
		StringTokenizer linesGrammar;
		String line;
		int lineCont = 1;
		String stateName;
		String contextProduces;
		String contextProducesArray[];
		
		String finalControllStateName = getNextFinalStateNameControll();
		
		linesGrammar = new StringTokenizer(grammar.getGrammarStructure(),"\n");
		
		while (linesGrammar.hasMoreElements()) {
			
			  line = linesGrammar.nextToken();
			  line = line.replaceAll("\\s", "");
			  if(line.isEmpty()) continue;
			  
			  stateName = String.valueOf(line.charAt(0));
			  contextProduces = line.substring(line.indexOf("=") + 1, line.length()).trim();
			  contextProducesArray = contextProduces.split("\\|");
			  
			  state = new State(stateName, (lineCont == 1), (contextProduces.contains("&")));
			  statesList.add(state);
			  
			  for (int i = 0; i < contextProducesArray.length; i++) {
				  
				  if(contextProducesArray[i].length() == 1 && !contextProducesArray[i].equals("&")){
					  
					  if(!existFinalControllState(statesList)){
						  state = new State(finalControllStateName, false, true);
						  state.setFinalControllState(true);
						  statesList.add(state);
					  }
					  
					  break;
				  }
			  }
			  lineCont++;
		}
		return statesList;
	}
	
	private String getNextFinalStateNameControll() {
		
		List<String> listStateName = getAllStateNameForGrammar();
		Map<Integer, String> alphabetMap = CharacterUtils.getAlphabetMap();
		
		boolean findNewStateName = false;
		String newStateName = "";
		int letterNumber = 1;
		
		while (!findNewStateName) {
		
			newStateName = alphabetMap.get(letterNumber);
			findNewStateName = !existStateWithName(listStateName, newStateName);
			letterNumber++;
		}
		
		return newStateName;
	}
	
	private boolean existFinalControllState(List<State> statesList) {
		for (State state : statesList) {
			if(state.isFinalControllState()) return true;
		}
		return false;
	}
	
	private boolean existStateWithName(List<String> listStateName, String stateName) {
		for (String state : listStateName) {
			if(state.equals(stateName)) return true;
		}
		return false;
	}
	
	private List<String> getAllStateNameForGrammar() {
		StringTokenizer linesGrammar;
		String stateName; 
		String line;
		List<String> listStateNameGrammar = new ArrayList<>();
		
		linesGrammar = new StringTokenizer(grammar.getGrammarStructure(),"\n");
		
		while (linesGrammar.hasMoreElements()) {
			
			  line = linesGrammar.nextToken();
			  stateName = String.valueOf(line.charAt(0));
			  listStateNameGrammar.add(stateName);
			  
		}
		return listStateNameGrammar;
	}
	
	private void verifyGrammarType(List<State> statesList) {
		if(ValidationsUtils.isCollectionNullOrEmpty(statesList)) return;
		
		List<Character> stateSimbolsList;
		grammar.setAFND(false);
		
		for (State state : statesList) {
			
			stateSimbolsList = new ArrayList<Character>();
			for (Transition transition : state.getTransitions()) {
				for (Character simbol : transition.getSimbols()) {
					stateSimbolsList.add(simbol);
				}
			}
			
			for (Character simbol : stateSimbolsList) {
				if(Collections.frequency(stateSimbolsList, simbol) > 1){
					grammar.setAFND(true);
					break;
				}
			}
			
		}
	}
	
	public String getHtmlGrammarWithNewStates(List<State> listStates, Grammar grammar) {
		
		String controlStateName = getFinalControllStateName(listStates);
		String lineBuilder;
		StringBuilder grammarBuilder = new StringBuilder();
		StringTokenizer linesGrammar = new StringTokenizer(grammar.getGrammarStructure(),"\n");
		char[] validTerminalSimbols = CharacterUtils.getValidTerminalSimbols();
		char[] validNonTerminalSimbols = CharacterUtils.getValidNonTerminalSimbols();
		String line;
		boolean foundEquals;
		
		while (linesGrammar.hasMoreElements()) {
			
			  line = linesGrammar.nextToken();
			  if(line.isEmpty()) continue;
			  
			  foundEquals = false;
			  lineBuilder = "";
			  
			  for (int index = 0; index < line.length(); index++) {
				  
				  int nextIndex = (index + 1);
				  char character = line.charAt(index);
				  char nextCharacter = ' ';
				  lineBuilder += String.valueOf(character);
				  
				  if(character == '='){
					  foundEquals = true;
				  }
				  if(!foundEquals) continue;
				  
				  if(nextIndex < line.length()){
					  nextCharacter = line.charAt(nextIndex);  
				  }
				  
				  boolean foundTerminalSimbol = ArrayUtils.contains(validTerminalSimbols, character);
				  boolean foundNonTerminalSimbol = ArrayUtils.contains(validNonTerminalSimbols, nextCharacter);
				  
				  if(foundTerminalSimbol && !foundNonTerminalSimbol){
					  lineBuilder = setTagFinalControllState(controlStateName, lineBuilder);
				  }
				  
			  }
			  
			  grammarBuilder.append(lineBuilder);
			  grammarBuilder.append("<br>");
		}
		
		grammarBuilder.insert(0, "<html>");
		grammarBuilder.insert(grammarBuilder.length(), getTagFinalControllState(controlStateName));
		grammarBuilder.insert(grammarBuilder.length(), "</html>");
		return grammarBuilder.toString();
	}
	
	private String getTagFinalControllState(String controlStateName) {
		if(controlStateName.isEmpty()) return "";
		return "<b><font color=\"#CC0000\">" + controlStateName + " = &</font></b>"; 
	}
	
	private String setTagFinalControllState(String controlStateName, String lineBuilder) {
		String html = "<b><font color=\"#CC0000\">" + controlStateName + "</font></b>"; 
		return lineBuilder += html;
	}
	
	public Grammar getGrammar() {
		return grammar;
	}

	public void setGrammar(Grammar grammar) {
		this.grammar = grammar;
	}
	
}
