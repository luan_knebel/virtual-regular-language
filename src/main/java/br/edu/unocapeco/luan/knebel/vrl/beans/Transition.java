package br.edu.unocapeco.luan.knebel.vrl.beans;

import java.util.TreeSet;

public class Transition {

    private TreeSet< Character > simbols;
    private State destinyState;
    
    public Transition(TreeSet< Character > simbols, State destinyState) {
        this.simbols = simbols;
        this.destinyState = destinyState;
    }

    public State getDestinyState() {
        return destinyState;
    }

    public void setDestinyState(State destinyState) {
		this.destinyState = destinyState;
	}

	public TreeSet< Character > getSimbols() {
        return simbols;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transition other = (Transition) obj;
        if (this.simbols != other.simbols && (this.simbols == null || !this.simbols.equals(other.simbols))) {
            return false;
        }
        if (this.destinyState != other.destinyState && (this.destinyState == null || !this.destinyState.equals(other.destinyState))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + ( this.simbols != null ? this.simbols.hashCode() : 0 );
        hash = 83 * hash + ( this.destinyState != null ? this.destinyState.hashCode() : 0 );
        return hash;
    }

    public String generatedStringSimbols() {

        StringBuilder sb = new StringBuilder();
        for ( Character c : simbols ) {
            sb.append( c + ", " );
        }

        if ( simbols.size() == 0 ) {
            return "";
        } else {
            return sb.toString().substring( 0, sb.toString().length() - 2 );
        }
        
    }

}
