package br.edu.unocapeco.luan.knebel.vrl.architecture.enums;

public enum EnumAutomataExceptionMessage {

	INVALID_SENTENCE("Sentença inválida!"),
	FINAL_STATE_NOT_FOUND("Estado final não encontrado!");
	
	private String message;
	
	EnumAutomataExceptionMessage(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
}
