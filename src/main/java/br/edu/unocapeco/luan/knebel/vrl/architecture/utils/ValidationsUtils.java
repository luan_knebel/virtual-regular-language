package br.edu.unocapeco.luan.knebel.vrl.architecture.utils;

import java.util.Collection;

public class ValidationsUtils {
	
	public static boolean isStringNullOrEmpty(String string){
		return (string == null || string.isEmpty());
	}
	
	public static boolean isStringNotNullOrEmpty(String string){
		if(string == null) return false;
		return (!string.isEmpty());
	}
	
	public static boolean isObjectNull(Object object){
		return (object == null);
	}
	
	public static boolean isObjectNotNull(Object object){
		return (object != null);
	}
	
	public static <T> boolean isCollectionNotNullOrEmpty(Collection<T> collection){
		if(collection == null) return false;
		return (collection.size() != 0);
	}
	
	public static <T> boolean isCollectionNullOrEmpty(Collection<T> collection){
		return (collection == null || collection.size() == 0);
	}

}
