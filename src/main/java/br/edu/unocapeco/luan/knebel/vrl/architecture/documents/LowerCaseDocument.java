package br.edu.unocapeco.luan.knebel.vrl.architecture.documents;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.commons.lang3.ArrayUtils;

public class LowerCaseDocument extends PlainDocument { 
	
	private static final long serialVersionUID = 1L;
	
	private int maxLength;
	
	public LowerCaseDocument(int maxLength) {
		super();
		this.maxLength = maxLength;
	}

	public LowerCaseDocument() {
		this.maxLength = 0;
	}
	
	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		
		if ((!ArrayUtils.contains(CaractersValidationDocument.getValidCaractersTerminals(), str)) && str.length() == 1) {
			str = null;
        }
		
		if(maxLength > 0 && offs > maxLength){
			str = null;
		}
		
        if (str == null) {
            return;
        }
        super.insertString(offs, str.toLowerCase(), a);
    }
}