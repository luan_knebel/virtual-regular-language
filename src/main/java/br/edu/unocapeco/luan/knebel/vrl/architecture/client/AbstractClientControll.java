package br.edu.unocapeco.luan.knebel.vrl.architecture.client;

import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.OptionPaneUtils;

public abstract class AbstractClientControll {

	private IClientDataControll clientDataControll;
	
	private IClientButtonControll clientButtonControll;
	
	public AbstractClientControll() {}
	
	public AbstractClientControll(IClientDataControll clientDataControll) {
		this.clientDataControll = clientDataControll;
	}

	public IClientDataControll getClientDataControll() {
		return clientDataControll;
	}

	public void setClientDataControll(IClientDataControll clientDataControll) {
		this.clientDataControll = clientDataControll;
	}
	
	public IClientButtonControll getClientButtonControll() {
		return clientButtonControll;
	}

	public void setClientButtonControll(IClientButtonControll clientButtonControll) {
		this.clientButtonControll = clientButtonControll;
	}

	public void printMessageDialog(String message) {
		OptionPaneUtils.printMessageDialog(getClientDataControll().getClientParent(), message);
	}
	
}
