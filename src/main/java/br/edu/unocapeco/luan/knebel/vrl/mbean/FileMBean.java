package br.edu.unocapeco.luan.knebel.vrl.mbean;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.ini4j.Wini;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractClientControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.client.IClientDataControll;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumFileExceptionMessage;
import br.edu.unocapeco.luan.knebel.vrl.architecture.exceptions.FileException;
import br.edu.unocapeco.luan.knebel.vrl.architecture.utils.ValidationsUtils;
import br.edu.unocapeco.luan.knebel.vrl.beans.Grammar;

public class FileMBean extends AbstractClientControll{

	private File file;
	private Grammar grammar = new Grammar();
	private JFileChooser fileChooser;

	private static final String SIMBOLS = "SIMBOLS";
	private static final String GRAMMAR = "GRAMMAR";
	private static final String Terminals = "Terminals";
	private static final String NonTerminals = "NonTerminals";
	private static final String Grammar = "Grammar";

	public FileMBean(IClientDataControll clientControll) {
		super(clientControll);
		initChooser();
	}

	private void initChooser() {
		emptyObjects();
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Grammar", "grammar");
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(filter);
	}

	public boolean readFile() throws FileException {

		validateSaveFile();

		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue != JFileChooser.APPROVE_OPTION)
			return false;

		try {
			file = fileChooser.getSelectedFile();
			readData(file);
			return true;
		} catch (Exception e) {
			emptyObjects();
			throw new FileException(EnumFileExceptionMessage.READ_ERROR, e);
		}

	}

	public void saveFile() throws FileException {
		if(ValidationsUtils.isObjectNotNull(file)){
			validateSaveFile();
			return;
		}
		validateFileData();
		
		if(!createNewFileToSaveDialog()) return;
		
		writeData(file);
	}
	
	public void validateSaveFileOnClosing() throws FileException{
		if(!validateMandatoryFiels()) return;
		if(!printSaveQuestionDialog()) return;
		validateFileData();
		if(ValidationsUtils.isObjectNull(file))
			if(!createNewFileToSaveDialog()) return;
		writeData(file);
	}
	
	public void validateSaveFile() throws FileException {
		if(ValidationsUtils.isObjectNull(file)) return;
		
		if(!printSaveQuestionDialog()) {
			return;
		}
		
		validateFileData();
		writeData(file);
	}
	
	public void newFile() throws FileException {
		validateSaveFile();
		emptyObjects();
	}

	private void readData(File file) throws FileException {
		if(ValidationsUtils.isObjectNull(file)) return;
		Wini ini = null;
		try {
			ini = new Wini(file);
		} catch (Exception e) {
			emptyObjects();
			throw new FileException(EnumFileExceptionMessage.READ_ERROR, e);
		}
		grammar.setTerminalSimbols(ini.get(SIMBOLS, Terminals));
		grammar.setNonTerminalSimbols(ini.get(SIMBOLS, NonTerminals));
		grammar.setGrammarStructure(ini.get(GRAMMAR, Grammar));
		grammar.setGrammarStructure(grammar.getGrammarStructure().replaceAll("#", "\n"));
	}

	private void writeData(File file) throws FileException {
		if(ValidationsUtils.isObjectNull(file)) return;
		Wini ini = null;
		try {
			if(!file.exists())
				file.createNewFile();
			ini = new Wini(file);
		} catch (Exception e) {
			emptyObjects();
			throw new FileException(EnumFileExceptionMessage.WRITE_ERROR, e);
		}
		
		grammar.setGrammarStructure(grammar.getGrammarStructure().replaceAll("#", ""));
		grammar.setGrammarStructure(grammar.getGrammarStructure().replaceAll("\n", "#"));
		
		ini.put(SIMBOLS, Terminals, grammar.getTerminalSimbols());
		ini.put(SIMBOLS, NonTerminals, grammar.getNonTerminalSimbols());
		ini.put(GRAMMAR, Grammar, grammar.getGrammarStructure());
		try {
			ini.store();
			JOptionPane.showMessageDialog(null, "Arquivo salvo!", "Informação", JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			emptyObjects();
			throw new FileException(EnumFileExceptionMessage.WRITE_ERROR, e);
		}
	}
	
	private boolean createNewFileToSaveDialog() {
		int returnValue = fileChooser.showSaveDialog(null);
		if(returnValue != JFileChooser.APPROVE_OPTION) return false;
		
		String pathFile = fileChooser.getSelectedFile().getAbsolutePath();
		
		if(!pathFile.endsWith(".grammar")) {
			pathFile += ".grammar";
		}
		
		file = new File(pathFile);
		return true;
	}
	
	private boolean printSaveQuestionDialog() {
		int returnQuestionValue = JOptionPane.YES_NO_OPTION;
		returnQuestionValue = JOptionPane.showConfirmDialog(null, "Deseja salvar o seu arquivo?","Salvar arquivo",JOptionPane.YES_NO_OPTION);
		return (returnQuestionValue == JOptionPane.YES_OPTION); 
	}
		
	private void validateFileData() throws FileException {
		if(!validateMandatoryFiels())
			throw new FileException(EnumFileExceptionMessage.DATA_EMPTY);
	}
	
	private boolean validateMandatoryFiels() {
		return (ValidationsUtils.isStringNotNullOrEmpty(grammar.getTerminalSimbols()) && ValidationsUtils.isStringNotNullOrEmpty(grammar.getNonTerminalSimbols()) && ValidationsUtils.isStringNotNullOrEmpty(grammar.getGrammarStructure()));
	}
	
	private void emptyObjects() {
		grammar = new Grammar();
		file = null;
	}

	public Grammar getGrammar() {
		return grammar;
	}

	public void setGrammar(Grammar grammar) {
		this.grammar = grammar;
	}

}
