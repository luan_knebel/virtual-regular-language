package br.edu.unocapeco.luan.knebel.vrl.architecture.enums;

public enum EnumGrammarExceptionMessage {

	GRAMMAR_DATA_EMPTY("Informe os dados para validar a gramática!"),
	INVALID_CARACTER_GRAMMAR("Existem caracteres inválidos na gramática!"),
	INVALID_CARACTER_TERMINAL_FIELD_GRAMMAR("Existem caracteres inválidos ou mal formados no campo de terminais da gramática!"),
	INVALID_CARACTER_NON_TERMINAL_FIELD_GRAMMAR("Existem caracteres inválidos ou mal formados no campo de não terminais da gramática!"),
	NON_TERMINAL_NOT_CONTAINED_FIELD_GRAMMAR("A gramática possui um não terminal que não está contido na lista de não terminais!"),
	SIMBOL_EQUALS_NOT_FOUND("Estrutura da gramática inválida!"),
	INVALID_CARACTERS_PRODUCES("Existem caracteres não contidos da lista de símbolos ou inválidos nas produções da gramática!"),
	INVALID_GRAMMAR("Gramática inválida"),
	NON_TERMINAL_GRAMMAR_DUPLICATED("Existe um não terminal duplicado na gramática!"),
	NON_TERMIAL_NOT_USED("Existe um não terminal não usado nas produções da gramática!"),
	GRAMMAR_NOT_VALIDATED("Valide a gramática para gerar o autômato!");
	
	private String message;
	
	EnumGrammarExceptionMessage(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
}
