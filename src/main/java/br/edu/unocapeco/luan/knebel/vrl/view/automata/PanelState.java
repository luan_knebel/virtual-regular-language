
package br.edu.unocapeco.luan.knebel.vrl.view.automata;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractPanel;
import br.edu.unocapeco.luan.knebel.vrl.architecture.enums.EnumTypeAutomata;
import br.edu.unocapeco.luan.knebel.vrl.beans.State;
import br.edu.unocapeco.luan.knebel.vrl.mbean.VirtualRegularLanguageMBean;

public class PanelState extends AbstractPanel {

	private static final long serialVersionUID = 1L;
	
	private State state;
    private int widthArrow;
    private boolean clicked;
    private EnumTypeAutomata typeAutomata;
    
    public PanelState(State state, VirtualRegularLanguageMBean mBean, EnumTypeAutomata typeAutomata) {
        super(mBean);
        this.state = state;
        this.typeAutomata = typeAutomata;
        
        if (state.isInitial())
            widthArrow = 20;
        else
            widthArrow = 0;
        
        setSize( new Dimension( 50 + widthArrow, 50 ) );
        setBackground( new Color( 0, 0, 0, 0 ) );
        initComponents();
    }

    private void initComponents() {

        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                formMouseReleased(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400, Short.MAX_VALUE));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 300, Short.MAX_VALUE));
    }

	private void formMouseDragged(java.awt.event.MouseEvent evt) {
		
	    if (getParent() != null && getParent().getMousePosition() != null) {
	        
	        int x = (int) getParent().getMousePosition().getX() - getWidth() / 2;
	        int y = (int) getParent().getMousePosition().getY() - getHeight() / 2;
	
	        if (getX() + getWidth() > getParent().getWidth())
	            x = getParent().getWidth() - getWidth() - 2;
	        if (getX() < 0)
	            x = 0;
	
	        if (getY() + getHeight() > getParent().getHeight())
	            y = getParent().getHeight() - getHeight() - 2;
	        if (getY() < 0)
	            y = 0;
	
	        setLocation(x, y);
	
	        if (state.isInitial()) {
	            state.setXCentral(x + (widthArrow / 2 ) + (getWidth() / 2));
	            state.setYCentral(y + (getHeight() / 2));
	        } else {
	            state.setXCentral(x + ( getWidth() / 2));
	            state.setYCentral(y + ( getHeight() / 2));
	        }
	
	        getParent().repaint();
	    
	    }
	}
	
	private void formMousePressed(java.awt.event.MouseEvent evt) {
		if(typeAutomata == EnumTypeAutomata.AFND){
			getManageBean().selectStatePanelAFNDAutomata(state);
		}else{
			getManageBean().selectStatePanelAFDAutomata(state);
		}
	    clicked = true;
	    repaint();
	}
	
	private void formMouseReleased(java.awt.event.MouseEvent evt) {
	    clicked = false;
	}

    @Override
    protected void paintComponent( Graphics g ) {
        super.paintComponent( g );
        
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        if (clicked) 
            g2d.setPaint(new Color(220, 245, 255));
        else
            g2d.setPaint(Color.WHITE);
        
        g2d.fill(new Ellipse2D.Double(widthArrow, 0, this.getWidth() - widthArrow, getHeight()));
        
        if (clicked) 
            g2d.setPaint(new Color(0, 102, 153));
        else
            g2d.setPaint(Color.BLACK);
        
        g2d.draw(new Ellipse2D.Double(widthArrow, 0, this.getWidth() - (widthArrow + 1), getHeight() - 1));
        
        if (state.isInitial()) {
            g2d.draw(new Line2D.Double(0, getHeight() / 2, widthArrow, getHeight() / 2));
            g2d.draw(new Line2D.Double(widthArrow - 5, ( getHeight() / 2) - 5, widthArrow, getHeight() / 2 ));
            g2d.draw(new Line2D.Double(widthArrow - 5, ( getHeight() / 2) + 5, widthArrow, getHeight() / 2 ));
        }
        
        if (state.isFinal() ) {
            g2d.draw(new Ellipse2D.Double(widthArrow + 5, 5, this.getWidth() - (widthArrow + 11), getHeight() - 11));
        }
        
        FontMetrics fm = g2d.getFontMetrics();
        g2d.drawString(state.getName(), widthArrow + (50 / 2) - (fm.stringWidth(state.getName()) / 2), (getHeight() / 2) + (fm.getHeight() / 3));
        
    }
    
    public State getState() {
        return state;
    }

	public EnumTypeAutomata getTypeAutomata() {
		return typeAutomata;
	}

	public void setTypeAutomata(EnumTypeAutomata typeAutomata) {
		this.typeAutomata = typeAutomata;
	}
    
}
