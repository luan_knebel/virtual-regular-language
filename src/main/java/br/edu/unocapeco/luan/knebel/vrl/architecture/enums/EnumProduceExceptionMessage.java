package br.edu.unocapeco.luan.knebel.vrl.architecture.enums;

public enum EnumProduceExceptionMessage {

	PRODUCES_EMPTY("Está faltando produções na gramática!"),
	INVALID_CARACTERS_PRODUCES("Existem caracteres não contidos da lista de símbolos ou inválidos nas produções da gramática!"),
	INVALID_PRODUCE("Produção inválida!"),
	INVALID_PRODUCE_TERMINAL("O primeiro símbolo da produção deverá ser um símbolo terminal!"),
	INVALID_PRODUCE_NON_TERMINAL_FOUND_GRAMMAR("Símbolo não terminal da produção não encontrado da gramática!"),
	INVALID_GRAMMAR("Gramática inválida");
	
	private String message;
	
	EnumProduceExceptionMessage(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
}
