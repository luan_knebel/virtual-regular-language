package br.edu.unocapeco.luan.knebel.vrl.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.edu.unocapeco.luan.knebel.vrl.architecture.client.AbstractFrame;
import br.edu.unocapeco.luan.knebel.vrl.view.automata.PanelViewAutomata;

public class FrameVirtualRegularLanguage extends AbstractFrame {

	private static final long serialVersionUID = 1L;

	private PanelVirtualRegularLanguage panelMain;

	public FrameVirtualRegularLanguage() {
		initComponents();
		initFormConfigurations();
		setLocationRelativeTo(null);
	}

	private void initFormConfigurations() {
		Dimension dimension = new Dimension(900, 500);
		setPreferredSize(dimension);
		setMinimumSize(dimension);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		add(panelMain, BorderLayout.CENTER);
		pack();
		getManageBean().setClientDataControll(this);
		getManageBean().setClientButtonControll(this);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				getManageBean().validateClosingWindow();
			}
		});
	}
	
	private void initComponents() {
		panelMain = new PanelVirtualRegularLanguage(getManageBean());

		JMenuBar rootMenu = new JMenuBar();
		JMenu menu = new JMenu();
		JMenuItem menuItem;
		
		menuItem = new JMenuItem("Novo");
		menuItem.addActionListener(action -> newAction());
		menu.add(menuItem);

		menuItem = new JMenuItem("Abrir");
		menuItem.addActionListener(action -> openAction());
		menu.add(menuItem);

		menuItem = new JMenuItem("Salvar");
		menuItem.addActionListener(action -> getManageBean().saveAction());
		menu.add(menuItem);

		menuItem = new JMenuItem("Sobre");
		menuItem.addActionListener(action -> getManageBean().aboutAction());
		menu.add(menuItem);

		menuItem = new JMenuItem("Sair");
		menuItem.addActionListener(action -> exitApplication());
		menu.add(menuItem);
		
		menu.setText("Arquivo");
		rootMenu.add(menu);

		menu = new JMenu("Ajuda");
		menuItem = new JMenuItem("Ajuda");
		menuItem.addActionListener(action -> getManageBean().helpAction());
		menu.add(menuItem);
		
		rootMenu.add(menu);
		setJMenuBar(rootMenu);
	}
	
	private void exitApplication() {
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
	
	private void newAction() {
		getManageBean().newAction();
		panelMain.resetTabbedPane();
	}
	
	private void openAction() {
		getManageBean().openAction();
		panelMain.resetTabbedPane();
	}
	
	@Override
	public PanelViewAutomata getPanelViewAutomata() {
		return panelMain.getPanelViewAutomata();
	}

	@Override
	public JTextField getTextFieldTerminals() {
		return panelMain.getTextFieldTerminals();
	}

	@Override
	public JTextField getTextFieldNotTerminals() {
		return panelMain.getTextFieldNotTerminals();
	}

	@Override
	public JTextArea getTextAreaGrammar() {
		return panelMain.getTextAreaGrammar();
	}

	@Override
	public String getSentence() {
		return panelMain.getSentence();
	}

	@Override
	public JTextArea getTextAreaConsoleValidations() {
		return panelMain.getTextAreaConsoleValidations();
	}

	@Override
	public PanelViewAutomata getPanelViewAutomataGrammar() {
		return panelMain.getPanelViewAutomataGrammar();
	}

	@Override
	public JTable getGrammarTable() {
		return panelMain.getGrammarTable();
	}

	@Override
	public JTable getAFDTable() {
		return panelMain.getAFDTable();
	}

	@Override
	public JTable getAFDSimplifiedTable() {
		return panelMain.getAFDSimplifiedTable();
	}
	
	@Override
	public Container getClientParent() {
		return getContentPane();
	}
	
	@Override
	public void setInformationsModel(){
		panelMain.setInformationsModel();
	}

	@Override
	public JEditorPane getEditorPanelGramar() {
		return panelMain.getEditorPanelGramar();
	}

	@Override
	public void setButtonValidateGrammarEnabled(Boolean enabled) {
		panelMain.setButtonValidateGrammarEnabled(enabled);
	}

	@Override
	public void setButtonCreateGrammarModifiedEnabled(Boolean enabled) {
		panelMain.setButtonCreateGrammarModifiedEnabled(enabled);
	}

	@Override
	public void setButtonGenerateTableEnabled(Boolean enabled) {
		panelMain.setButtonGenerateTableEnabled(enabled);
	}

	@Override
	public void setButtonGenerateAutomataTableEnabled(Boolean enabled) {
		panelMain.setButtonGenerateAutomataTableEnabled(enabled);
	}

	@Override
	public void setButtonTableAFNDEnabled(Boolean enabled) {
		panelMain.setButtonTableAFNDEnabled(enabled);
	}

	@Override
	public void setButtonTableAFNDSimplifiedEnabled(Boolean enabled) {
		panelMain.setButtonTableAFNDSimplifiedEnabled(enabled);
	}

	@Override
	public void setButtonGenerateAutomataValidateEnabled(Boolean enabled) {
		panelMain.setButtonGenerateAutomataValidateEnabled(enabled);
	}

	@Override
	public void setButtonValidateSentencesEnabled(Boolean enabled) {
		panelMain.setButtonValidateSentencesEnabled(enabled);
	}
}
